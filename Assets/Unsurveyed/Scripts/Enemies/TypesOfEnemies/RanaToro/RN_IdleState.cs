﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RN_IdleState : IdleState
{
    private RanaToro enemy;

    public RN_IdleState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_IdleState statedata, RanaToro enemy) : base(entity, stateMachine, animboolname, statedata)
    {
        this.enemy = enemy;

    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (IsIdleTimeOver == true /*|| entity.rb.velocity.x!=0*/)
        {
            stateMachine.ChangeState(enemy.moveState);
        }
        if (entity.fov.DetectPlayer == true && enemy.CheckWall()==false && enemy.CheckLedge()==true)
        {
            stateMachine.ChangeState(enemy.chargeState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
