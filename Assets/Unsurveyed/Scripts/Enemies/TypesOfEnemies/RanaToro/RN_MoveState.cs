﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RN_MoveState : MoveState
{
    private RanaToro Enemy;

    public RN_MoveState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_MoveState statedata, RanaToro enemy) : base(entity, stateMachine, animboolname, statedata)
    {
        this.Enemy = enemy;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (Self.transform.localScale.x < 0)
        {

            Self.transform.Translate(Left * StateData.MovementSpeed * Time.deltaTime, Space.Self);

            //rb.MovePosition(Self.transform.position + (Vector3.left * Time.deltaTime));

            //Debug.Log("me muevo a la izquierda");
        }
        else if (Self.transform.localScale.x > 0)
        {
            Self.transform.Translate(Right * StateData.MovementSpeed * Time.deltaTime, Space.Self);
            //Debug.Log("me muevo a la derecha");
        }

        if (IsDetectingWall || IsDetectingLedge == false)
        {
            stateMachine.ChangeState(Enemy.idleState);
            Enemy.idleState.SetFlipAfter(true);
        }
        if (entity.fov.DetectPlayer == true)
        {
            stateMachine.ChangeState(Enemy.chargeState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
