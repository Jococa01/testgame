﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RN_ChargeState : ChargeState
{
    protected RanaToro Enemy;
   
    public RN_ChargeState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_ChargeState statedata, RanaToro enemy) : base(entity, stateMachine, animboolname, statedata)
    {
        this.Enemy = enemy;
    }

    public override void Enter()
    {
        base.Enter();
        ChargeDir = entity.AliveGO.transform.localScale.x;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        GroundCheck = entity.CheckLedge();
        WallCheck = entity.CheckWall();
        ChargeDir = entity.AliveGO.transform.localScale.x;

        if (GroundCheck == true && WallCheck == false)
        {
            entity.AliveGO.transform.Translate(new Vector2(entity.AliveGO.transform.localScale.x, 0) * StateData.ChargeSpeed * Time.deltaTime, Space.Self);
        }
        else if(WallCheck==true || GroundCheck==false)
        {
            stateMachine.ChangeState(Enemy.idleState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
