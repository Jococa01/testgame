﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RanaToro : Entity
{
    public RN_IdleState idleState { get; private set; }
    public RN_MoveState moveState { get; private set; }
    public RN_ChargeState chargeState { get; private set; }

    [SerializeField]
    private D_IdleState idlestatedata;
    [SerializeField]
    private D_MoveState movestatedata;
    [SerializeField]
    private D_ChargeState chargestatedata;

    public override void Start()
    {
        base.Start();

        moveState = new RN_MoveState(this, stateMachine, "move", movestatedata, this);
        idleState = new RN_IdleState(this, stateMachine, "idle", idlestatedata, this);
        chargeState = new RN_ChargeState(this, stateMachine, "charge", chargestatedata, this);

        stateMachine.initialize(moveState);

    }
}
