﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FL_IdleState : IdleState
{
    private Flor enemy;
    private bool Detected;
    public FL_IdleState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_IdleState statedata, Flor enemy) : base(entity, stateMachine, animboolname,statedata)
    {
        this.enemy = enemy;
    }
    public override void Enter()
    {
        base.Enter();
        Transform paco = entity.AliveGO.transform.Find("Tronco_Inferior/Rotate");
        paco.rotation = Quaternion.Euler(0, 0, 62f);
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        //Vector3 Relativepos = entity.fov.VisibleTargets[0].transform.position - entity.AliveGO.transform.position;
        //float Pos = Relativepos.normalized.x;

        if (entity.fov.DetectPlayer==true)
        {
            Debug.Log("Detectado");
            stateMachine.ChangeState(enemy.shootState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
