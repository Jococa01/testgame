﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flor : Entity
{
    public FL_IdleState idleState { get; private set; }
    public FL_ShootState shootState { get; private set; }

    [SerializeField]
    private D_IdleState idlestatedata;
    [SerializeField]
    private D_ShootState shootstatedata;

    public override void Start()
    {
        base.Start();

        shootState = new FL_ShootState(this, stateMachine, "shoot", shootstatedata, this);
        idleState = new FL_IdleState(this, stateMachine, "idle", idlestatedata, this);

        stateMachine.initialize(idleState);

    }
}
