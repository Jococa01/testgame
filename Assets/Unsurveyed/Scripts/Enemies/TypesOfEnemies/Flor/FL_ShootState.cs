﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FL_ShootState : ShootState
{
    private Flor enemy;
    public FL_ShootState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_ShootState statedata, Flor enemy) : base(entity, stateMachine, animboolname, statedata)
    {
        this.enemy = enemy;
    }
    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (entity.fov.DetectPlayer == false)
        {
            Debug.Log("Dejo de ver al jugador");
            stateMachine.ChangeState(enemy.idleState);
        }
        else
        {
            Vector3 Relativepos = entity.fov.VisibleTargets[0].transform.position - entity.AliveGO.transform.position;
            float Pos = Relativepos.normalized.x;
            Transform paco = entity.AliveGO.transform.Find("Tronco_Inferior/Rotate");

            Vector3 dir = entity.fov.VisibleTargets[0].position - paco.position;
            float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            paco.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);

            if (Pos > 0)
            {
                entity.XDir = 1f;
            }
            if (Pos < 0)
            {
                entity.XDir = -1f;
            }
        }

    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public void Shoot()
    {
        GameObject Shoot = GameObject.Find("ShotPos");
        Rigidbody2D Proyectilrb = entity.Proyectil.GetComponent<Rigidbody2D>();
        Object.Instantiate(entity.Proyectil, Shoot.transform.position, Quaternion.Euler(new Vector3(0,0, Shoot.transform.eulerAngles.z)), entity.transform).SetActive(true);
    }
}
