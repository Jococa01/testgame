﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NF_AttackState : AttackState
{
    private Necromorfo Enemy;
    private float Timer;

    public NF_AttackState(Entity entity, FiniteStateMachine stateMachine, string animboolname,D_AttackState statedata, Necromorfo enemy) : base(entity, stateMachine, animboolname, statedata)
    {
        this.Enemy = enemy;
    }

    public override void Enter()
    {
        base.Enter();
        Timer = 2f;
    }

    public override void Exit()
    {
        base.Exit();       
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        Timer -= Time.deltaTime;
        if (CanAttack==false)
        {
            if (Timer <= 0.0f)
            {
                stateMachine.ChangeState(Enemy.moveState);
            }
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
