﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NF_MoveState : MoveState
{
    private Necromorfo Enemy;

    public NF_MoveState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_MoveState statedata, Necromorfo enemy) : base(entity, stateMachine, animboolname, statedata)
    {
        this.Enemy = enemy;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        //IsDetectingLedge = entity.CheckLedge();
        //IsDetectingWall = entity.CheckWall();

        if (FOV.DetectPlayer == true && IsDetectingLedge == true && IsDetectingWall == false)
        {
            if (PlatformerCharacter2D.crouched == false)
            {
                CanChase = true;
                if (entity.inputHandler.Grounded == false)
                {
                    entity.rb.bodyType = RigidbodyType2D.Dynamic;
                    entity.rb.simulated = true;
                }
                else
                {
                    entity.rb.bodyType = RigidbodyType2D.Kinematic;
                    entity.rb.simulated = true;
                }
            }
            else if (PlatformerCharacter2D.crouched == true)
            {
                if (entity.inputHandler.Grounded == true)
                {
                    entity.rb.bodyType = RigidbodyType2D.Kinematic;
                    entity.rb.simulated = false;
                }
                else if (PlatformerCharacter2D.TouchingNecro == false)
                {
                    entity.rb.bodyType = RigidbodyType2D.Dynamic;
                    entity.rb.simulated = true;
                }
            }
        }
        else
        {
            CanChase = false;
            if (entity.inputHandler.Grounded == false)
            {
                entity.rb.bodyType = RigidbodyType2D.Dynamic;
                entity.rb.simulated = true;
            }
            else
            {
                entity.rb.bodyType = RigidbodyType2D.Kinematic;
                entity.rb.simulated = false;
            }

        }

        if (PlatformerCharacter2D.crouched == false)
        {
            entity.rb.bodyType = RigidbodyType2D.Dynamic;
            entity.rb.simulated = true;
        }

        if (CanChase == false)
        {
            entity.anim.SetBool("chase", false);
            if (Self.transform.localScale.x < 0)
            {

                Self.transform.Translate(Left * StateData.MovementSpeed * Time.deltaTime, Space.Self);

                //rb.MovePosition(Self.transform.position + (Vector3.left * Time.deltaTime));

                //Debug.Log("me muevo a la izquierda");
            }
            else if (Self.transform.localScale.x > 0)
            {
                Self.transform.Translate(Right * StateData.MovementSpeed * Time.deltaTime, Space.Self);
                //Debug.Log("me muevo a la derecha");
            }
            //if (IsDetectingLedge)
            //{
            //    Debug.Log("Toco Suelo");
            //}
        }

        else
        {
            entity.anim.SetBool("chase", true);
            if (Self.transform.localScale.x < 0)
            {
                Self.transform.Translate(Left * 2 * Time.deltaTime, Space.Self);
                Debug.Log("me muevo a la izquierda");
            }
            else if (Self.transform.localScale.x > 0)
            {
                Self.transform.Translate(Right * 2 * Time.deltaTime, Space.Self);
                Debug.Log("me muevo a la derecha");
            }

            var currentdistance = Vector2.Distance(entity.AliveGO.transform.position, Player.transform.position);
            if (currentdistance <= ATD)
            {
                CanAttack = true;
            }
        }

        if (IsDetectingWall || IsDetectingLedge == false)
        {
            stateMachine.ChangeState(Enemy.idleState);
            Enemy.idleState.SetFlipAfter(true);
        }
        if (CanAttack == true)
        {
            stateMachine.ChangeState(Enemy.attackState);
        }

    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
