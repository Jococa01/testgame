﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NF_IdleState : IdleState
{
    private Necromorfo enemy;

    public NF_IdleState(Entity entity, FiniteStateMachine stateMachine, string animboolname,D_IdleState statedata, Necromorfo enemy) : base(entity, stateMachine, animboolname, statedata)
    {
        this.enemy = enemy;

    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    { 
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (IsIdleTimeOver==true)
        {
            stateMachine.ChangeState(enemy.moveState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
