﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Necromorfo : Entity
{
    public NF_IdleState idleState { get; private set; }
    public NF_MoveState moveState { get; private set; }
    public NF_AttackState attackState { get; private set; }

    [SerializeField]
    private D_IdleState idlestatedata;
    [SerializeField]
    private D_MoveState movestatedata;
    [SerializeField]
    private D_AttackState attackstatedata;

    public override void Start()
    {
        base.Start();

        moveState = new NF_MoveState(this, stateMachine, "move", movestatedata, this);
        idleState = new NF_IdleState(this, stateMachine, "idle", idlestatedata, this);
        attackState = new NF_AttackState(this, stateMachine, "attack", attackstatedata, this);

        stateMachine.initialize(moveState);

    }

}
