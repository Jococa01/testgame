﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State
{
    protected FiniteStateMachine stateMachine;
    protected Entity entity;
    protected string AnimBoolName;

    protected float startTime;

    public State(Entity entity, FiniteStateMachine stateMachine, string animboolname)
    {
        this.entity = entity;
        this.stateMachine = stateMachine;
        this.AnimBoolName = animboolname;
    }

    public virtual void Enter()
    {
        startTime = Time.time;
        entity.anim.SetBool(AnimBoolName, true);
    }
    public virtual void Exit()
    {
        entity.anim.SetBool(AnimBoolName, false);
    }
    public virtual void LogicUpdate()
    {

    }
    public virtual void PhysicsUpdate()
    {

    }
}
