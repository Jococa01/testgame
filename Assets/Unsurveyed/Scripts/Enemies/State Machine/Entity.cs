﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public D_Entity entitydata;

    public FiniteStateMachine stateMachine;

    public float FacingDirection { get; private set; }

    public Rigidbody2D rb { get; private set; }

    public Animator anim { get; private set; }

    public GameObject AliveGO { get; private set; }

    public FOV fov { get; private set; }

    public PlayerInputHandler inputHandler { get; private set; }

    public float XDir;

    private Vector3 FixedLook;

    private Vector2 VelocityWorkspace;

    [SerializeField]
    private Transform WallCheck;
    [SerializeField]
    private Transform LedgeCheck;
    [SerializeField]
    public GameObject Proyectil;


    public virtual void Start()
    {
        AliveGO = transform.GetChild(0).gameObject;
        inputHandler = AliveGO.GetComponent<PlayerInputHandler>();
        FacingDirection = AliveGO.transform.localScale.x;
        XDir = FacingDirection;
        rb = AliveGO.GetComponent<Rigidbody2D>();
        anim = AliveGO.GetComponent<Animator>();
        stateMachine = new FiniteStateMachine();
        fov = AliveGO.GetComponent<FOV>();

        FixedLook = new Vector3(XDir, 1, 1);
        AliveGO.transform.localScale = FixedLook;
    }

    public virtual void Update()
    {
        stateMachine.CurrentState.LogicUpdate();

        XDir = AliveGO.transform.localScale.x;
        AliveGO.transform.localScale = FixedLook;
    }
    public virtual void FixedUpdate()
    {
        stateMachine.CurrentState.PhysicsUpdate();
    }

    public virtual void SetVelocity(float velocity)
    {
        VelocityWorkspace.Set(FacingDirection * velocity, rb.velocity.y);
        //rb.velocity = VelocityWorkspace;
    }
    public virtual bool CheckWall()
    {
        return Physics2D.Raycast(WallCheck.position, Vector2.right*XDir, entitydata.WallCheckDistance, entitydata.WhatIsGround);
    }
    public virtual bool CheckLedge()
    {
        return Physics2D.Raycast(LedgeCheck.position, Vector2.down, entitydata.LedgeCheckDistance, entitydata.WhatIsGround);
    }
    public virtual void Flip()
    {
        FacingDirection *= -1f;
        XDir *= -1f;
        FixedLook = new Vector3(XDir, 1, 1);
        //XDir *=-XDir;
    }

    public virtual void CheckPlayer(bool PlayerDetected)
    {
        PlayerDetected = fov.DetectPlayer;
    }

    public virtual void OnDrawGizmos()
    {
        Gizmos.DrawLine(WallCheck.position, WallCheck.position + (Vector3)(Vector2.right * XDir * entitydata.WallCheckDistance));
        Gizmos.DrawLine(LedgeCheck.position, LedgeCheck.position + (Vector3)(Vector2.down * entitydata.LedgeCheckDistance));
    }
    public virtual void PlaySound(string sound)
    {
        FindObjectOfType<Audiomanager>().Play(sound);
    }
}
