﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : State
{
    protected D_AttackState StateData;
    protected Transform Player;
    protected float currentdistance;
    protected bool CanAttack;
    protected float atkd;

    public AttackState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_AttackState statedata) : base(entity, stateMachine, animboolname)
    {
        this.StateData = statedata;
    }

    public override void Enter()
    {
        base.Enter();
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        CanAttack = true;
        atkd = StateData.AttackDistance;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        Debug.Log("ataco");
        currentdistance = Vector2.Distance(entity.AliveGO.transform.position, Player.transform.position);
        if (currentdistance > atkd)
        {
            Debug.Log("no ataco");
            CanAttack = false;
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
