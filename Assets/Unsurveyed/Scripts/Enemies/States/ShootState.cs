﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootState : State
{
    protected D_ShootState StateData;



    public ShootState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_ShootState statedata) : base(entity, stateMachine, animboolname)
    {
        this.StateData = statedata;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
