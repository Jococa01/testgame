﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State
{
    protected D_IdleState StateData;

    protected bool flipAfter;
    protected bool IsIdleTimeOver;

    protected float IdleTime;


    public IdleState(Entity entity, FiniteStateMachine stateMachine, string animboolname,D_IdleState statedata) : base(entity, stateMachine, animboolname)
    {
        this.StateData = statedata;
    }

    public override void Enter()
    {
        base.Enter();
        //entity.AliveGO.GetComponent<FOV>().Target = LayerMask.NameToLayer("Default");
        entity.SetVelocity(0f);
        IsIdleTimeOver = false;
        SetRandomIdleTime();
    }

    public override void Exit()
    {
        base.Exit();
        if (flipAfter)
        {
            entity.Flip();
            //entity.AliveGO.GetComponent<FOV>().Target = LayerMask.GetMask("Jugador");
        }
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if(Time.time>=startTime + IdleTime)
        {
            IsIdleTimeOver = true;
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
    public void SetFlipAfter(bool flip)
    {
        flipAfter = flip;
    }
    private void SetRandomIdleTime()
    {
        IdleTime = Random.Range(StateData.MinIdleTime, StateData.MaxIdleTime);
    }
}
