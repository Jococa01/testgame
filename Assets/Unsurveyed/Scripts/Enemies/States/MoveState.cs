﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveState : State
{
    protected D_MoveState StateData;
    protected bool IsDetectingWall;
    protected bool IsDetectingLedge;
    protected GameObject Self, Player;
    protected Vector2 Right = new Vector2(1, 0);
    protected Vector2 Left = new Vector2(-1, 0);
    protected FOV FOV;
    protected bool CanChase = false;

    //attack mechanic
    protected bool CanAttack;
    protected float ATD;


    public MoveState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_MoveState statedata) : base(entity, stateMachine, animboolname)
    {
        this.StateData = statedata;
    }

    public override void Enter()
    {
        base.Enter();
        entity.SetVelocity(StateData.MovementSpeed);
        IsDetectingLedge = entity.CheckLedge();
        IsDetectingWall = entity.CheckWall();
        Self = entity.AliveGO.gameObject;

        FOV = Self.GetComponent<FOV>();
        Player = GameObject.FindGameObjectWithTag("Player");
        CanAttack = false;
        ATD = 1.3f;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();


        IsDetectingLedge = entity.CheckLedge();
        IsDetectingWall = entity.CheckWall();
    }


    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();

    }
}
