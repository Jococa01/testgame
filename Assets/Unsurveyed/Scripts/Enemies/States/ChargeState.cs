﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeState : State
{
    protected D_ChargeState StateData;
    protected float ChargeDir;
    protected bool GroundCheck, WallCheck;

    public ChargeState(Entity entity, FiniteStateMachine stateMachine, string animboolname, D_ChargeState statedata) : base(entity, stateMachine, animboolname)
    {
        this.StateData = statedata;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
