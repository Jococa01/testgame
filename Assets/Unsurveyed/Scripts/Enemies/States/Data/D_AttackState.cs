﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMoveStateData", menuName = "Data/State Data/Attack State")]
public class D_AttackState : ScriptableObject
{
    public float AttackDistance = 1;
}
