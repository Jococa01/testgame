﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Database", menuName ="Assets/Databases/Item Database")]
public class ItemDatabase : ScriptableObject
{
    public List<Item> AllItems;
}
