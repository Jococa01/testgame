﻿using UnityEngine;

[CreateAssetMenu(fileName ="New Item", menuName = "Assets/Items/New Item")]
public class Item : ScriptableObject
{
    public string ItemName;
    public int ItemID;
}
