﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndDemo : MonoBehaviour
{
    public GameObject fade;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(8f);
        fade.GetComponent<Animator>().SetTrigger("Start");
        yield return new WaitForSeconds(3f);
        Application.Quit();
        Debug.Log("End");
    }
}
