﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler,IEndDragHandler,IDragHandler
{
    [SerializeField] private Canvas canvas;
    private RectTransform rectTransform;
    public CanvasGroup canvasGroup;
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        GameObject CG = GameObject.Find("Canvas");
        canvas= CG.GetComponent<Canvas>();
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("Empiezo a arrastrar");
    }
    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("Arrastro");
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
        rectTransform.anchoredPosition += eventData.delta/canvas.scaleFactor;
        transform.localScale = new Vector3(1,1,1);
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("Dejo de arrastrar");
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Cliqueo");
    }
}
