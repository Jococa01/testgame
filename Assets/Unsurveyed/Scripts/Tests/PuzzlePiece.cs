﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzlePiece : MonoBehaviour
{
    public Item Item;
    public int ItemValue;
    public PuzzleMenu menu;
    public GameObject Button1, Button2;

    private void Awake()
    {
        ItemValue = Item.ItemID;
        Button1.GetComponent<Button>().enabled = false;
        Button2.GetComponent<Button>().enabled = false;
    }
    private void OnDestroy()
    {
        //Debug.Log("Destroyed");
        if(ItemValue>=1 && ItemValue <=4)
        {
            Button1.GetComponent<Button>().enabled = true;
            Button1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/PuzzleHandle/1");
        }
        if (ItemValue >= 5 && ItemValue <= 8)
        {
            Button2.GetComponent<Button>().enabled = true;
            Button2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/PuzzleHandle/1");
        }
        if (ItemValue == 1)
        {
            PuzzleMenu.Pieza1Activa = true;
            Debug.Log("He cogido la primera pieza");

        }
        if (ItemValue == 2)
        {
            PuzzleMenu.Pieza2Activa = true;
            Debug.Log("Pieza2");

        }
        if (ItemValue == 3)
        {
            PuzzleMenu.Pieza3Activa = true;
            Debug.Log("Pieza3");

        }
        if (ItemValue == 4)
        {
            PuzzleMenu.Pieza4Activa = true;
            Debug.Log("Pieza4");

        }
        if (ItemValue == 5)
        {
            PuzzleMenu.Pieza5Activa = true;
            Debug.Log("He cogido la quinta pieza");

        }
        if (ItemValue == 6)
        {
            PuzzleMenu.Pieza6Activa = true;
            Debug.Log("Pieza6");

        }
        if (ItemValue == 7)
        {
            PuzzleMenu.Pieza7Activa = true;
            Debug.Log("Pieza7");

        }
        if (ItemValue == 8)
        {
            PuzzleMenu.Pieza8Activa = true;
            Debug.Log("Pieza8");

        }
        else if(ItemValue>12)
        {
            Debug.Log("null value");
        }
    }
}
