﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resolucion : MonoBehaviour
{
    public GameObject[] PieceIDs;
    public GameObject[] SocketIDs;
    public Dictionary<string, bool> NamedBools = new Dictionary<string, bool>();
    public static int CurrentPuzzle;

    //Forma no optimizada
    public static bool Puzzle1Completed = false, Puzzle2Completed = false, Puzzle3Completed = false, Puzzle4Completed=false;

    public void Awake()
    {
        
    }
    void Update()
    {
        if (PuzzleMenu.update == true)
        {
            UpdatePieces();
        }
        if (PieceIDs.Length >=4)
        {
            PuzzleMenu puzzleMenu = FindObjectOfType<PuzzleMenu>();

            if (PieceIDs[0].transform.position == SocketIDs[0].transform.position && PieceIDs[1].transform.position == SocketIDs[1].transform.position && PieceIDs[2].transform.position == SocketIDs[2].transform.position && PieceIDs[3].transform.position == SocketIDs[3].transform.position && CurrentPuzzle==0 )
            {
                Debug.Log("Puzzle Completado");
                Puzzle1Completed = true;
                puzzleMenu.ResetPuzzlePieces();
                puzzleMenu.GetImage(0);
            }
            if (PieceIDs[0].transform.position == SocketIDs[0].transform.position && PieceIDs[1].transform.position == SocketIDs[1].transform.position && PieceIDs[2].transform.position == SocketIDs[2].transform.position && PieceIDs[3].transform.position == SocketIDs[3].transform.position && CurrentPuzzle == 1)
            {
                Debug.Log("Puzzle Completado");
                Puzzle2Completed = true;
                puzzleMenu.ResetPuzzlePieces();
                puzzleMenu.GetImage(1);
            }
        }

        //Boolean array
        //if (PieceIDs.Length !=0)
        //{
        //    for (int i = 0; i < 4; i++)
        //    {
        //        NamedBools.Add(("Puzzle nº " + i), false);
        //    }
        //    foreach (KeyValuePair<string, bool> entry in NamedBools)
        //    {
        //        Debug.Log(entry);
        //    }
        //}


    }

    public void UpdatePieces()
    {
        PieceIDs = GameObject.FindGameObjectsWithTag("PuzzlePiece");
    }
}
