﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PuzzleMenu : MonoBehaviour
{
    public Image FinishedIMG;
    public Sprite[] Sprites;
    public List<RectTransform> PuzzlePos = new List<RectTransform>();
    public GameObject PiecesParent, PZBG;
    private int arraynumber=0;
    public static bool update = false;

    [Header("Detección de piezas individuales")]
    public Dictionary<string, bool> PickedPiece = new Dictionary<string, bool>();

    //Forma No optimizada
    public static bool Pieza1Activa = false, Pieza2Activa = false, Pieza3Activa = false, Pieza4Activa = false, Pieza5Activa = false, Pieza6Activa = false, Pieza7Activa = false, Pieza8Activa = false;

    private void Awake()
    {
        //ResetPuzzlePieces();
    }

    // Update is called once per frame
    void Update()
    {

        
    }
    
    public void GetImage(int ImageID)
    {
        Resolucion.CurrentPuzzle = ImageID;

        switch (ImageID)
        {
            case 3:
                Debug.Log("4a Imagen");
                FinishedIMG.sprite = Resources.Load<Sprite>("Images/FinishedPuzzle/4");
                Sprites = Resources.LoadAll("Images/PuzzlePieces/Puzzle4", typeof(Sprite)).Cast<Sprite>().ToArray();
                foreach (Sprite sprite in Sprites)
                {
                    GameObject PiezaPuzzle = new GameObject("Pieza nº " + Sprites[arraynumber]);
                    PiezaPuzzle.transform.parent = PiecesParent.transform;
                    PiezaPuzzle.layer = 5;
                    PiezaPuzzle.tag = "PuzzlePiece";
                    PiezaPuzzle.AddComponent<RectTransform>();
                    PiezaPuzzle.AddComponent<CanvasGroup>();
                    PiezaPuzzle.AddComponent<DragDrop>();
                    PiezaPuzzle.AddComponent<Image>();
                    PiezaPuzzle.GetComponent<Image>().sprite = sprite;
                    Vector3 x = Vector3.one;
                    PiezaPuzzle.GetComponent<RectTransform>().localScale = x;
                    PuzzlePos.Add(PiezaPuzzle.GetComponent<RectTransform>());
                    arraynumber++;
                    update = true;
                }
                //posición de cada cosica
                for (int i = 0; i <= arraynumber; i++)
                {
                    switch (i)
                    {
                        case 4:
                            PuzzlePos[4].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 400, PuzzlePos[4].rect.width);
                            PuzzlePos[4].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[4].rect.height);
                            break;
                        case 3:
                            PuzzlePos[3].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 300, PuzzlePos[3].rect.width);
                            PuzzlePos[3].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -0, PuzzlePos[3].rect.height);
                            break;
                        case 2:
                            PuzzlePos[2].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 200, PuzzlePos[2].rect.width);
                            PuzzlePos[2].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[2].rect.height);
                            break;
                        case 1:
                            PuzzlePos[1].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 100, PuzzlePos[1].rect.width);
                            PuzzlePos[1].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[1].rect.height);
                            break;
                        default:
                            Debug.Log(";D");
                            break;
                    }
                }
                break;
            case 2:
                Debug.Log("3a Imagen");
                FinishedIMG.sprite = Resources.Load<Sprite>("Images/FinishedPuzzle/3");
                Sprites = Resources.LoadAll("Images/PuzzlePieces/Puzzle3", typeof(Sprite)).Cast<Sprite>().ToArray();
                foreach (Sprite sprite in Sprites)
                {
                    GameObject PiezaPuzzle = new GameObject("Pieza nº " + Sprites[arraynumber]);
                    PiezaPuzzle.transform.parent = PiecesParent.transform;
                    PiezaPuzzle.layer = 5;
                    PiezaPuzzle.tag = "PuzzlePiece";
                    PiezaPuzzle.AddComponent<RectTransform>();
                    PiezaPuzzle.AddComponent<CanvasGroup>();
                    PiezaPuzzle.AddComponent<DragDrop>();
                    PiezaPuzzle.AddComponent<Image>();
                    PiezaPuzzle.GetComponent<Image>().sprite = sprite;
                    Vector3 x = Vector3.one;
                    PiezaPuzzle.GetComponent<RectTransform>().localScale = x;
                    PuzzlePos.Add(PiezaPuzzle.GetComponent<RectTransform>());
                    arraynumber++;
                    update = true;
                }
                //posición de cada cosica
                for (int i = 0; i <= arraynumber; i++)
                {
                    switch (i)
                    {
                        case 4:
                            PuzzlePos[4].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 400, PuzzlePos[4].rect.width);
                            PuzzlePos[4].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[4].rect.height);
                            break;
                        case 3:
                            PuzzlePos[3].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 300, PuzzlePos[3].rect.width);
                            PuzzlePos[3].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -0, PuzzlePos[3].rect.height);
                            break;
                        case 2:
                            PuzzlePos[2].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 200, PuzzlePos[2].rect.width);
                            PuzzlePos[2].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[2].rect.height);
                            break;
                        case 1:
                            PuzzlePos[1].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 100, PuzzlePos[1].rect.width);
                            PuzzlePos[1].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[1].rect.height);
                            break;
                        default:
                            Debug.Log(";D");
                            break;
                    }
                }
                break;
            case 1:
                Debug.Log("2a Imagen");
                FinishedIMG.sprite = Resources.Load<Sprite>("Images/FinishedPuzzle/2");
                Sprites = Resources.LoadAll("Images/PuzzlePieces/Puzzle2", typeof(Sprite)).Cast<Sprite>().ToArray();

                if (Resolucion.Puzzle2Completed == false)
                {
                    FinishedIMG.gameObject.SetActive(false);
                    PZBG.SetActive(true);

                    foreach (Sprite sprite in Sprites)
                    {
                        GameObject PiezaPuzzle = new GameObject("Pieza nº " + Sprites[arraynumber]);
                        PiezaPuzzle.transform.parent = PiecesParent.transform;
                        PiezaPuzzle.layer = 5;
                        PiezaPuzzle.tag = "PuzzlePiece";
                        PiezaPuzzle.AddComponent<RectTransform>();
                        PiezaPuzzle.AddComponent<CanvasGroup>();
                        PiezaPuzzle.AddComponent<DragDrop>();
                        PiezaPuzzle.AddComponent<Image>();
                        PiezaPuzzle.GetComponent<Image>().sprite = sprite;
                        Vector3 x = Vector3.one;
                        PiezaPuzzle.GetComponent<RectTransform>().localScale = x;
                        PuzzlePos.Add(PiezaPuzzle.GetComponent<RectTransform>());
                        arraynumber++;
                        update = true;
                    }
                    //posición de cada cosica
                    for (int i = 0; i <= arraynumber; i++)
                    {
                        switch (i)
                        {
                            case 3:
                                PuzzlePos[3].sizeDelta = new Vector2(105, 105);
                                //PuzzlePos[3].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 450, PuzzlePos[3].rect.width);
                                //PuzzlePos[3].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[3].rect.height);
                                PuzzlePos[3].localPosition = new Vector3(0, -175);
                                Vector2 Localpivot3 = new Vector2(0.605f, 0.39f);
                                PuzzlePos[3].GetComponent<RectTransform>().pivot = Localpivot3;
                                if (Pieza8Activa == false)
                                {
                                    PuzzlePos[3].gameObject.SetActive(false);
                                }
                                else if (Pieza8Activa == true)
                                {
                                    PuzzlePos[3].gameObject.SetActive(true);
                                }
                                break;
                            case 2:
                                PuzzlePos[2].sizeDelta = new Vector2(105, 105);
                                //PuzzlePos[2].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 300, PuzzlePos[2].rect.width);
                                //PuzzlePos[2].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -0, PuzzlePos[2].rect.height);
                                PuzzlePos[2].localPosition = new Vector3(0, -50);
                                Vector2 Localpivot2 = new Vector2(0.391f, 0.391f);
                                PuzzlePos[2].GetComponent<RectTransform>().pivot = Localpivot2;
                                if (Pieza7Activa == false)
                                {
                                    PuzzlePos[2].gameObject.SetActive(false);
                                }
                                else if (Pieza7Activa == true)
                                {
                                    PuzzlePos[2].gameObject.SetActive(true);
                                }
                                break;
                            case 1:
                                PuzzlePos[1].sizeDelta = new Vector2(105, 105);
                                //PuzzlePos[1].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 150, PuzzlePos[1].rect.width);
                                //PuzzlePos[1].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[1].rect.height);
                                PuzzlePos[1].localPosition = new Vector3(0, 75);
                                Vector2 Localpivot1 = new Vector2(0.609f, 0.383f);
                                PuzzlePos[1].GetComponent<RectTransform>().pivot = Localpivot1;
                                if (Pieza6Activa == false)
                                {
                                    PuzzlePos[1].gameObject.SetActive(false);
                                }
                                else if (Pieza6Activa == true)
                                {
                                    PuzzlePos[1].gameObject.SetActive(true);
                                }
                                break;
                            case 0:
                                PuzzlePos[0].sizeDelta = new Vector2(105, 105);
                                //PuzzlePos[0].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, PuzzlePos[0].rect.width);
                                //PuzzlePos[0].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, PuzzlePos[0].rect.height);
                                PuzzlePos[0].localPosition = new Vector3(0, 200);
                                Vector2 Localpivot = new Vector2(0.62f, 0.61f);
                                PuzzlePos[0].GetComponent<RectTransform>().pivot = Localpivot;
                                if (Pieza5Activa == false)
                                {
                                    PuzzlePos[0].gameObject.SetActive(false);
                                }
                                else if (Pieza5Activa == true)
                                {
                                    PuzzlePos[0].gameObject.SetActive(true);
                                }
                                break;
                            default:
                                //foreach(KeyValuePair<string,bool> dicinfo in PickedPiece)
                                //{
                                //    Debug.Log(dicinfo.Key + "," + dicinfo.Value);
                                //}    
                                break;
                        }
                    }
                }
                if (Resolucion.Puzzle2Completed == true)
                {
                    FinishedIMG.gameObject.SetActive(true);
                    PZBG.SetActive(false);
                }
                break;
            case 0:

                Debug.Log("1a Imagen");
                FinishedIMG.sprite = Resources.Load<Sprite>("Images/FinishedPuzzle/1");
                Sprites = Resources.LoadAll("Images/PuzzlePieces/Puzzle1", typeof(Sprite)).Cast<Sprite>().ToArray();

                if (Resolucion.Puzzle1Completed == false)
                {
                    FinishedIMG.gameObject.SetActive(false);
                    PZBG.SetActive(true);

                    foreach (Sprite sprite in Sprites)
                    {
                        GameObject PiezaPuzzle = new GameObject("Pieza nº " + Sprites[arraynumber]);
                        PiezaPuzzle.transform.parent = PiecesParent.transform;
                        PiezaPuzzle.layer = 5;
                        PiezaPuzzle.tag = "PuzzlePiece";
                        PiezaPuzzle.AddComponent<RectTransform>();
                        PiezaPuzzle.AddComponent<CanvasGroup>();
                        PiezaPuzzle.AddComponent<DragDrop>();
                        PiezaPuzzle.AddComponent<Image>();
                        PiezaPuzzle.GetComponent<Image>().sprite = sprite;
                        Vector3 x = Vector3.one;
                        PiezaPuzzle.GetComponent<RectTransform>().localScale = x;
                        PuzzlePos.Add(PiezaPuzzle.GetComponent<RectTransform>());
                        update = true;
                        arraynumber++;
                    }

                    //posición de cada cosica
                    for (int i = 0; i <= arraynumber; i++)
                    {
                        switch (i)
                        {
                            case 3:
                                PuzzlePos[3].sizeDelta = new Vector2(105, 105);
                                //PuzzlePos[3].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, PuzzlePos[3].rect.width);
                                //PuzzlePos[3].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -50, PuzzlePos[3].rect.height);
                                PuzzlePos[3].localPosition = new Vector3(0, -175);
                                Vector2 Localpivot3 = new Vector2(0.5f, 0.391f);
                                PuzzlePos[3].GetComponent<RectTransform>().pivot = Localpivot3;
                                if (Pieza4Activa == false)
                                {
                                    PuzzlePos[3].gameObject.SetActive(false);
                                }
                                else if (Pieza4Activa == true)
                                {
                                    PuzzlePos[3].gameObject.SetActive(true);
                                }
                                break;
                            case 2:
                                PuzzlePos[2].sizeDelta = new Vector2(105, 105);
                                //PuzzlePos[2].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, PuzzlePos[2].rect.width);
                                //PuzzlePos[2].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -150, PuzzlePos[2].rect.height);
                                PuzzlePos[2].localPosition = new Vector3(0, -50);
                                Vector2 Localpivot2 = new Vector2(0.391f, 0.391f);
                                PuzzlePos[2].GetComponent<RectTransform>().pivot = Localpivot2;
                                if (Pieza3Activa == false)
                                {
                                    PuzzlePos[2].gameObject.SetActive(false);
                                }
                                else if (Pieza3Activa == true)
                                {
                                    PuzzlePos[2].gameObject.SetActive(true);
                                }
                                break;
                            case 1:
                                PuzzlePos[1].sizeDelta = new Vector2(105, 105);
                                //PuzzlePos[1].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, PuzzlePos[1].rect.width);
                                //PuzzlePos[1].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -250, PuzzlePos[1].rect.height);
                                PuzzlePos[1].localPosition = new Vector3(0, 75);
                                Vector2 Localpivot1 = new Vector2(0.609f, 0.383f);
                                PuzzlePos[1].GetComponent<RectTransform>().pivot = Localpivot1;
                                if (Pieza2Activa == false)
                                {
                                    PuzzlePos[1].gameObject.SetActive(false);
                                }
                                else if (Pieza2Activa == true)
                                {
                                    PuzzlePos[1].gameObject.SetActive(true);
                                }
                                break;
                            case 0:
                                PuzzlePos[0].sizeDelta = new Vector2(105, 105);
                                //PuzzlePos[0].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, PuzzlePos[0].rect.width);
                                //PuzzlePos[0].SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -350, PuzzlePos[0].rect.height);
                                PuzzlePos[0].localPosition = new Vector3(0, 200);
                                Vector2 Localpivot = new Vector2(0.507f, 0.609f);
                                PuzzlePos[0].GetComponent<RectTransform>().pivot = Localpivot;
                                if (Pieza1Activa == false)
                                {
                                    PuzzlePos[0].gameObject.SetActive(false);
                                }
                                else if (Pieza1Activa == true)
                                {
                                    PuzzlePos[0].gameObject.SetActive(true);
                                }
                                break;
                            default:
                                //foreach(KeyValuePair<string,bool> dicinfo in PickedPiece)
                                //{
                                //    Debug.Log(dicinfo.Key + "," + dicinfo.Value);
                                //}    
                                break;
                        }
                    }
                }
                if (Resolucion.Puzzle1Completed == true)
                {
                    FinishedIMG.gameObject.SetActive(true);
                    PZBG.SetActive(false);
                }
                break;

            default:
                Debug.Log("Null value");
                break;
        }
    }

    public void ResetPuzzlePieces()
    {
        int childs = PiecesParent.transform.childCount;
        for(int i = childs-1; i>= 0; i--)
        {
            Destroy(PiecesParent.transform.GetChild(i).gameObject);
            arraynumber = 0;
            PuzzlePos.Clear();
            //PickedPiece.Clear();
        }
        update = false;
    }
    
    void DictionaryValues()
    {
            //for (int i = 0; i < Sprites.Length; i++)
            //{
            //    PickedPiece.Add(("Pieza " + i), false);
            //}
            //foreach (KeyValuePair<string, bool> entry in PickedPiece)
            //{
            //    Debug.Log(entry);
            //}
    }
}
