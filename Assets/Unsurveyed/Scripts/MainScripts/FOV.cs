﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOV : MonoBehaviour
{
    public float Radiovision;
    [Range(0,360)]
    public float Angulovision;
    public LayerMask Target;
    public LayerMask Obstacles;
    public bool DetectPlayer = false;
    public bool inverted=false;

    public List<Transform> VisibleTargets = new List<Transform>();

    public Vector3 Direccionangular(float AngulosEnGrados, bool AnguloEsGlobal)
    {
        if (inverted == false)
        {
            if (!AnguloEsGlobal)
            {
                AngulosEnGrados += transform.eulerAngles.z;
            }
            return new Vector3(Mathf.Cos(AngulosEnGrados * Mathf.Deg2Rad), Mathf.Sin(AngulosEnGrados * Mathf.Deg2Rad), 0);
        }
        if (inverted == true)
        {
            if (!AnguloEsGlobal)
            {
                AngulosEnGrados += transform.eulerAngles.z;
            }
            return new Vector3(Mathf.Sin(AngulosEnGrados * Mathf.Deg2Rad), Mathf.Cos(AngulosEnGrados * Mathf.Deg2Rad), 0);
        }
        else
        {
            if (!AnguloEsGlobal)
            {
                AngulosEnGrados += transform.eulerAngles.z;
            }
            return new Vector3(Mathf.Cos(AngulosEnGrados * Mathf.Deg2Rad), Mathf.Sin(AngulosEnGrados * Mathf.Deg2Rad), 0);
        }
    }


    private void Start()
    {
        StartCoroutine("FindTargetsWithDelay", 0f);
    }

    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindTarget();
        }
    }

    void FindTarget()
    {

        VisibleTargets.Clear();

        Collider2D[] TargetsVisibles = Physics2D.OverlapCircleAll(transform.position, Radiovision, Target);
        for (int i = 0; i < TargetsVisibles.Length; i++)
        {
            Transform target = TargetsVisibles[i].transform;
            Vector2 DirTarget = (target.position - transform.position).normalized;
            if (Vector2.Angle(transform.right * transform.localScale.x, DirTarget) < Angulovision / 2)
            {
                float DistTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics2D.Raycast(transform.position, DirTarget, DistTarget, Obstacles))
                {
                    VisibleTargets.Add(target);
                    Debug.Log("Detecto Enemigo");
                    DetectPlayer = true;
                }
            }
        }
        if (VisibleTargets.Count == 0)
        {
            DetectPlayer = false;
        }
    }
}
