﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    public float ClimbSpeed = 1;
    public bool moving = false;
    public GameObject Player;
    public PlatformerCharacter2D PlayerController;
    public Animator animator;

    private void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        PlayerController = Player.GetComponent<PlatformerCharacter2D>();
        animator = Player.GetComponent<Animator>();
    }
    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "ClimbPoint")
        {
            animator.SetFloat("ClimbSpeed", Player.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("Climb", true);
            Player.GetComponent<PlatformerCharacter2D>().m_Grounded = true;
            animator.SetBool("Ground", true);
        }
        if (collision.transform.tag == "ClimbPoint" && Input.GetKey(KeyCode.W))
        {
            Player.transform.GetComponent<Rigidbody2D>().velocity = new Vector2(0, ClimbSpeed);
            moving = true;
            animator.SetBool("Climbing", true);
        }
        if (collision.transform.tag== "ClimbPoint" && Input.GetKey(KeyCode.S))
        {
            Player.transform.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -ClimbSpeed);
            moving = true;
            animator.SetBool("Climbing", true);
        }
        if(collision.transform.tag== "ClimbPoint" && moving==false)
        {
            //collision.transform.GetComponent<Rigidbody2D>().velocity = new Vector2(0, .57f);
            //collision.transform.GetComponent<Rigidbody2D>().isKinematic = true;
            Player.transform.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            animator.SetBool("Climbing", false);
        }
        else if(Input.GetKey(KeyCode.W)==false && Input.GetKey(KeyCode.S)==false)
        {
            moving = false;
            animator.SetBool("Climbing", false);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "ClimbPoint")
        {
            Player.transform.GetComponent<Rigidbody2D>().isKinematic = false;
            moving = false;
            animator.SetBool("Climb", false);
            animator.SetBool("Climbing", false);
        }
    }
}
