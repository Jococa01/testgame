using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.InputSystem;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        public PlatformerCharacter2D m_Character;
        public bool m_Jump, crouch;
        public float h;
        public Vector2 Axis = Vector2.zero;
        //[SerializeField] InputActionAsset controles;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }

        public void OnMovementInput(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Axis = context.ReadValue<Vector2>();
                h = Axis.x;
                // Pass all parameters to the character control script.
                //m_Character.Move(h, crouch, m_Jump);
            }
            if (context.canceled)
            {
                Axis = Vector2.zero;
                h = Axis.x;
                // Pass all parameters to the character control script.
                //m_Character.Move(h, crouch, m_Jump);
            }
        }

        private void FixedUpdate()
        {
            // Read the inputs.
            //bool crouch;
            if (Input.GetKey(KeyCode.LeftControl))
            {
                Debug.Log("Crouch");
                if (m_Character.dragging == false && m_Character.m_Grounded==true)
                {
                    crouch = true;
                }
                else
                {
                    crouch = false;
                }
            }
            else
            {
                crouch = false;
            }
            //Debug.Log(crouch);
            //h= CrossPlatformInputManager.GetAxis("Horizontal");
            //h = Axis.x;
            // Pass all parameters to the character control script.
            m_Character.Move(h, crouch, m_Jump);
            m_Jump = false;
        }
    }
}
