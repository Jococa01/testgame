﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelFinished : MonoBehaviour
{
    public GameObject[] PiecesArray;
    public TMPro.TMP_Text CompletitionText;
    public PuzzleMenu menu;
    public int NPieces;
    public GameObject canvas, loader, interacttext;
    public Audiomanager audiomng;
    private void Awake()
    {
        audiomng = FindObjectOfType<Audiomanager>();
        canvas.SetActive(false);
        //menu = FindObjectOfType<PuzzleMenu>();
        NPieces = 0;

        for(int i =0; i < PiecesArray.Length; i++)
        {
            PiecesArray[i].GetComponent<Image>().color = Color.gray;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            interacttext.SetActive(true);
            if (Input.GetKey(KeyCode.E))
            {
                //Pausing the game
                canvas.SetActive(true);
                Time.timeScale = 0;

                //Setting the pieces and the number
                if (Resolucion.Puzzle1Completed == true)
                {
                    PiecesArray[0].GetComponent<Image>().color = Color.white;
                    PiecesArray[1].GetComponent<Image>().color = Color.white;
                    PiecesArray[2].GetComponent<Image>().color = Color.white;
                    PiecesArray[3].GetComponent<Image>().color = Color.white;
                    NPieces = 4;
                }
                if (PuzzleMenu.Pieza1Activa == true)
                {
                    PiecesArray[0].GetComponent<Image>().color = Color.white;
                    NPieces++;
                }
                if (PuzzleMenu.Pieza2Activa == true)
                {
                    PiecesArray[1].GetComponent<Image>().color = Color.white;
                    NPieces++;
                }
                if (PuzzleMenu.Pieza3Activa == true)
                {
                    PiecesArray[2].GetComponent<Image>().color = Color.white;
                    NPieces++;
                }
                if (PuzzleMenu.Pieza4Activa == true)
                {
                    PiecesArray[3].GetComponent<Image>().color = Color.white;
                    NPieces++;
                }
                else
                {

                }
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            interacttext.SetActive(false);
        }
    }

    public void nextlevel(int time)
    {
        StartCoroutine(LeveLoader(time));
    }

    IEnumerator LeveLoader(int time)
    {
        audiomng.fade(0, 1f);
        loader.GetComponent<Animator>().SetTrigger("Start");
        yield return new WaitForSecondsRealtime(time);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        //Destroy(GameObject.Find("ChiefManager"));
        //Destroy(GameObject.Find("[Debug Updater]"));
    }
}
