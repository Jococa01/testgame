using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using System.Linq;

    public class PlatformerCharacter2D : MonoBehaviour
    {
        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
        [SerializeField] private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.
        [Range(0, 1)] [SerializeField] public float m_CrouchSpeed = .36f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
        [SerializeField] private bool m_AirControl = false;                 // Whether or not a player can steer while jumping;
        [SerializeField] public LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

        public Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
        const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
        public bool m_Grounded;            // Whether or not the player is grounded.
        public Transform m_CeilingCheck;   // A position marking where to check for ceilings
        const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
        public Animator m_Anim;            // Reference to the player's animator component.
        public Rigidbody2D m_Rigidbody2D;
        private bool m_FacingRight = true;  // For determining which way the player is currently facing.
        public static bool crouched;

        [Header("Variables for the swing and climb mechanic")]

        public bool IsAttached = false;
        private HingeJoint2D PHJ;
        public float PushForce = 10f;
        public Transform AttachedTo;
        private GameObject Disregard;

        [Header("Variables for the pulling & pushing mechanic")]

        public bool dragging = false;
        public float Distance=1f;
        public LayerMask BoxMask;
        GameObject box;
        public Rigidbody2D BoxRigy2d;
        public GameObject PickUpPoint;
        public float relativepos = 0;
        public bool OnLimit = false;

        [Header("Possession Mechanic")]
        GameObject Enemigo;
        public bool IsPossessing = false;
        public static float PossesionTime;
        public static bool WaitTimeIsOver;

        [SerializeField]
        public Sprite[] PossessedSprites;

        [Header("Inventory Menu")]

        public GameObject InvMenu;
        public bool IsPaused = false;
        public static int CoinCount=0, PuzzleCount=0;

    [Header("Interface, HP & other mechanics")]

        public static int HP, Stamina;
        public int MaxHP = 3, MaxStamina = 10;
        public UIManager UIManager;
        public GameObject Heart1, Heart2, Heart3;
        public bool Invulnerable = false, knockback=false;
        public static bool dead = false, TouchingNecro=false;
        public SpriteRenderer spriteRenderer;

        [Header("Audio")]
        public AudioSource source;
        public AudioClip[] clips;
        public int Mat = 0;
        public float CrouchedVolume = .2f * Audio.AudioMultiplyer;

        [Header("New Input Variables")]
        Vector2 MovementInput;

    //---------------------------------------------------------Main_Voids---------------------------------------------------------------------------//
    private void Awake()
        {
            dead = false;
            // Setting up references.
            source = GetComponent<AudioSource>();
            WaitTimeIsOver = true;
            m_GroundCheck = transform.Find("GroundCheck");
            m_CeilingCheck = transform.Find("CeilingCheck");
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
            PHJ = GetComponent<HingeJoint2D>();
            UIManager.SetMaxStamina(MaxStamina);
            HP = MaxHP;
            Stamina = MaxStamina;
            spriteRenderer = GetComponent<SpriteRenderer>();
        }


        private void FixedUpdate()
        {
            if (IsPossessing == false && IsPaused==false && m_Anim.GetBool("Climb")==false && dead==false)
            {
                m_Grounded = false;

                // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
                // This can be done using layers instead but Sample Assets will not overwrite your project settings.

            if(m_Anim.GetBool("Climb") == false)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject && m_Rigidbody2D.velocity.y < .1f && m_Rigidbody2D.velocity.y > -.1f)
                        m_Grounded = true;

                    if (colliders[i].gameObject.layer == 13)
                    {
                        Mat = 1;
                        //Debug.Log("Tocamos hierba");
                    }
                    if (colliders[i].gameObject.layer == 9)
                    {
                        Mat = 2;
                        //Debug.Log("Tocamos Piedra");
                    }
                }
            }
            else if(m_Anim.GetBool("Climb") == true)
            {
                m_Grounded = true;
            }
                m_Anim.SetBool("Ground", m_Grounded);
                // Set the vertical animation
                m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);

                checkInputs();
            }
        }

    private void Update()
    {
            if (IsPossessing == false && IsPaused==false && dead==false)
        {
            m_Anim.SetFloat("PushD", m_Rigidbody2D.velocity.x*relativepos);
            if (m_Grounded == false)
            {
                m_Anim.SetBool("IsPushing", false);
                if (dragging == true)
                {
                    box.GetComponent<FixedJoint2D>().enabled = false;
                    box.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                    dragging = false;
                }
                dragging = false;
            }
        }

            //Cambio de sprites etc..
            RaycastHit2D hit2d = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, 10f);
        if (hit2d.collider != null)
        {
            if (hit2d.collider.tag == "PossessableEnemy")
            {
                if(hit2d.collider.name == "Rana_Toro")
                {
                    hit2d.transform.GetComponent<SpriteRenderer>().sprite = PossessedSprites[0];
                }
            }
            else
            {
                GameObject[] paco = GameObject.FindGameObjectsWithTag("PossessableEnemy");
                foreach(GameObject enemy in paco)
                {
                    if (enemy.name == "Rana_Toro")
                    {
                        enemy.GetComponent<SpriteRenderer>().sprite = PossessedSprites[1];
                    }
                    else
                    {

                    }
                }
                //GameObject paco = GameObject.Find("Rana_Toro");
                //paco.GetComponent<SpriteRenderer>().sprite = PossessedSprites[1];
            }
        }
        else
        {

        }



        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    if (InvMenu.activeSelf == false)
        //    {
        //        InvMenu.SetActive(true);
        //        Time.timeScale = 0f;
        //        IsPaused = true;
        //    }
        //    else if (InvMenu.activeSelf == true)
        //    {
        //        InvMenu.SetActive(false);
        //        Time.timeScale = 1f;
        //        IsPaused = false;
        //    }
        //}
        if (crouched && m_Grounded && !dragging)
        {
            spriteRenderer.color = new Color(180f / 255f, 180f / 255f, 180f / 255f);
            Color MColor = spriteRenderer.color;
            MColor.a = .8f;
            spriteRenderer.color = MColor;

        }
        else if(!Invulnerable)
        {
            spriteRenderer.color = new Color(255f / 255f, 255f / 255f, 255f / 255f);
            Color MColor = spriteRenderer.color;
            MColor.a = 1f;
            spriteRenderer.color = MColor;
        }

        if (HP <= 0)
        {
            dead = true;
        }
        if (dead == true)
        {
            StartCoroutine(Death());
        }

        if(m_Anim.GetBool("Climb") == true)
        {
            m_MaxSpeed = 1;
        }
        if (m_Anim.GetBool("Climb") == false)
        {
            m_MaxSpeed = 5;
        }

    }

    //----------------------------------------------------------Voids--------------------------------------------------------------------------------//

    public void Move(float move, bool crouch, bool jump)
        {
        if (IsPossessing == false && dead==false &&knockback==false)
        {

            //only control the player if grounded or airControl is turned on
            if (m_Grounded || m_AirControl)
            {
                // Reduce the speed if crouching by the crouchSpeed multiplier
                move = (crouch ? move * m_CrouchSpeed : move);

                // The Speed animator parameter is set to the absolute value of the horizontal input.
                m_Anim.SetFloat("Speed", Mathf.Abs(move));

                // Move the character
                m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y);

                // If the input is moving the player right and the player is facing left...
                if (move > 0 && !m_FacingRight)
                {
                    if (dragging)
                    {
                        //Debug.Log("Derc");
                    }
                    if (dragging == false)
                    {
                        // ... flip the player.
                        Flip();
                    }
                }
                // Otherwise if the input is moving the player left and the player is facing right...
                else if (move < 0 && m_FacingRight)
                {
                    if (dragging)
                    {
                        //Debug.Log("Izq");
                    }
                    if (dragging == false)
                    {
                        // ... flip the player.
                        Flip();
                    }
                }
            }
        }
        }


        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.tag == "Vine" && !IsAttached)
            {
                if(AttachedTo != collision.gameObject.transform.parent)
                {
                    if(Disregard==null || collision.gameObject.transform.parent.gameObject != Disregard)
                    {
                    Attach(collision.gameObject.GetComponent<Rigidbody2D>());
                    }
                }
            }
            if (collision.transform.tag == "Coin")
            {
                Destroy(collision.gameObject);
                Debug.Log("He obtenido una moneda");
                CoinCount++;
            }
            if (collision.transform.tag == "Puzzle")
            {
                if(collision.gameObject.GetComponent<PuzzlePiece>().ItemValue > 12)
                {
                    Debug.Log("Null Value");
                }
                else
                {
                    Destroy(collision.gameObject);
                }
            }
        }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Limitador")
        {
            OnLimit = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Limitador")
        {
            OnLimit = false;
        }
    }

    // Void for the movement inside the "expeerimental" vines (Might delete later)

    void checkInputs()
        {
            if (Input.GetKey("a") || Input.GetKey("left") && IsAttached)
            {
                m_Rigidbody2D.AddRelativeForce(new Vector3(-1, 0, 0) * PushForce);
            }
            if (Input.GetKey("d") || Input.GetKey("right") && IsAttached)
            {
                m_Rigidbody2D.AddRelativeForce(new Vector3(1, 0, 0) * PushForce);
            }
            if (Input.GetKeyDown("w") || Input.GetKeyDown("up") && IsAttached)
            {
            slide(1);
            }
            if (Input.GetKeyDown("s") || Input.GetKeyDown("down") && IsAttached)
            {
            slide(-1);
            }
            if (Input.GetKey(KeyCode.Space) && IsAttached)
            {
            Detach();
            }
        }

        //Void for Attaching to the vines

        public void Attach(Rigidbody2D VineBone)
        {
        VineBone.gameObject.GetComponent<Segment>().isPlayerAttached = true;
        PHJ.connectedBody = VineBone;
        PHJ.enabled = true;
        IsAttached = true;
        AttachedTo = VineBone.gameObject.transform.parent;
        }

        //Void for Detaching from the vines

        public void Detach()
        {
        PHJ.connectedBody.gameObject.GetComponent<Segment>().isPlayerAttached = false;
        IsAttached = false;
        PHJ.enabled = false;
        PHJ.connectedBody = null;
        m_Rigidbody2D.AddRelativeForce(new Vector3(transform.localScale.x,transform.localScale.y/5,0) *500f);
        StartCoroutine(Detaching());
        }

        //Void for sliding on the vines

        public void slide(float Direction)
    {
            if (IsAttached)
            {
                Segment myconection = PHJ.connectedBody.gameObject.GetComponent<Segment>();
                 GameObject newsegment = null;
                if (Direction > 0)
                {
                    if (myconection.ConnectedAbove != null)
                 {
                     if (myconection.ConnectedAbove.gameObject.GetComponent<Segment>() != null)
                        {
                          newsegment = myconection.ConnectedAbove;
                        }
                    }
                }
                else
                {
                    if (myconection.ConnectedBelow != null)
                    {
                      newsegment = myconection.ConnectedBelow;
                    }
                }
                if (newsegment != null)
                {
                    transform.position = newsegment.transform.position;
                    myconection.isPlayerAttached = false;
                    newsegment.GetComponent<Segment>().isPlayerAttached = true;
                    PHJ.connectedBody = newsegment.GetComponent<Rigidbody2D>();
                }
            }
       }

        //Void dedicated to the damage infligement to the player & some other stuff related to the health and Stamina of the Player

        public void TakeDamage(Vector2 dirKN)
        {
            if (Invulnerable ==false)
            {
                Debug.Log("Recibo da�o");
                HP--;
                StartCoroutine(Knockback(dirKN));
                Debug.Log(HP);
                Invulnerable = true;          
                StartCoroutine(DamageCoolDown());

                if (HP <= 2)
                {
                    Heart3.SetActive(false);
                }
                if (HP <= 1)
                {
                    Heart2.SetActive(false);
                }
                if (HP == 0)
                {
                    Heart1.SetActive(false);
                }
                else if (HP < 0)
                {
                    Debug.Log("Ya est�s muerto");
                }
            }
        }

        private void OnDrawGizmos()
        {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.right * transform.localScale.x * 10);
        Gizmos.DrawWireSphere(m_CeilingCheck.transform.position, k_CeilingRadius);
        }

        public void PlaySound(int sound)
        {
            int RandomFSa = Random.Range(0, 80);
            int RandomFSb = Random.Range(80, 200);
            if (sound >=0 && sound <=5)
            {
                if (Mat == 1)
                {
                    sound = RandomFSa;
                }
                if (Mat == 2)
                {
                    sound = RandomFSb;
                }
                else if(Mat==0)
                {
                    Debug.Log("Material Inv�lido o no asignado");
                }
                source.clip = clips[sound];
                source.Play();
                if (crouched)
                {
                    source.volume = CrouchedVolume;
                }
                else
                {
                    source.volume = .4f * Audio.AudioMultiplyer;
                }
            }
            else
            {
                source.clip = clips[sound];
                source.Play();
                source.volume = 1f * Audio.AudioMultiplyer;
            }
        }

    public void OnJumpInput(InputAction.CallbackContext context)
    {
        if (IsPossessing==false && m_Anim.GetBool("Climb")==false)
        {
            if (context.started)
            {
                if(m_Grounded == true && m_Anim.GetBool("Ground"))
                {
                    m_Grounded = false;
                    m_Anim.SetBool("Ground", false);
                    m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
                    Debug.Log("Saltamos desde los nuevos inputs");
                }
            }
        }
    }
    public void OnCrouchInput(InputAction.CallbackContext context)
    {
        if (IsPossessing == false && m_Anim.GetBool("Climb") == false)
        {
            m_CrouchSpeed = .1f;

            if (context.started)
            {
                Debug.Log("Me Agacho");
                crouched = true;
            }
            if (context.canceled)
            {               
                    crouched = false;
                    Debug.Log("Me dejo de agachar");       
            }
            m_Anim.SetBool("Crouch", crouched);
        }
    }
    public void OnInteractionInput(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            //Creating The Pulling & Pushing mechanics
            //First I need to cast a ray to detect if the interactive objects are near to me

            Physics2D.queriesStartInColliders = false;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, Distance, BoxMask);

            if (hit.collider != null && OnLimit==false)
            {
                m_Anim.SetBool("IsPushing", true);
                box = hit.collider.gameObject;

                Vector3 relativevector = box.transform.position - transform.position;
                relativepos = relativevector.normalized.x;

                box.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                box.GetComponent<FixedJoint2D>().enabled = true;
                box.GetComponent<FixedJoint2D>().connectedBody = GetComponent<Rigidbody2D>();
                BoxRigy2d = box.GetComponent<Rigidbody2D>();
                box.transform.position =  new Vector3 (PickUpPoint.transform.position.x, box.transform.position.y, box.transform.position.z);
                dragging = !dragging;

                if (dragging == false)
                {
                    box.GetComponent<FixedJoint2D>().enabled = false;
                    box.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                    m_Anim.SetBool("IsPushing", false);
                }
            }
        }
    }
    public void OnPossessingInput(InputAction.CallbackContext context)
    {
        if (IsPossessing == false && knockback==false)
        {
            if (context.started)
            {
                RaycastHit2D hit2d = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, 10f);
                if (hit2d.collider != null)
                {
                    if (hit2d.collider.tag == "PossessableEnemy" && WaitTimeIsOver)
                    {
                        if (hit2d.collider.name == "Rana_Toro")
                        {
                            Enemigo = hit2d.collider.gameObject;
                            GetComponent<AudioListener>().enabled = false;
                            Enemigo.GetComponent<AudioListener>().enabled = true;
                            Enemigo.GetComponent<PlayerInput>().enabled = true;
                            IsPossessing = true;
                            Enemigo.GetComponent<RanaInputHandler>().IsPossessed = true;
                        }
                        //Enemigo = hit2d.collider.gameObject;
                        //GetComponent<AudioListener>().enabled = false;
                        //Enemigo.GetComponent<AudioListener>().enabled = true;
                        //Enemigo.GetComponent<PlayerInput>().enabled = true;
                        //IsPossessing = true;
                        //Enemigo.GetComponent<PlayerInputHandler>().IsPossessed = true;
                    }
                    else
                    {
                        Debug.Log("Enemigo inv�lido");
                    }
                }
                else
                {
                    Debug.Log("No se puede poseer nada");
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Spike")
        {
            TakeDamage(new Vector2(0,1));
        }
        if (collision.transform.tag == "Enemy" && m_Grounded==true && crouched==false)
        {
            TakeDamage(new Vector2(-transform.localScale.x,1));
        }
        if(collision.transform.tag == "PossessableEnemy")
        {
            TakeDamage(new Vector2(-transform.localScale.x, 1));
        }
        else if (collision.transform.tag == "Enemy"&& m_Grounded == false && crouched == false)
        {
            TakeDamage(new Vector2(-transform.localScale.x, 1));
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "Spike")
        {
            TakeDamage(new Vector2(0, 1));
        }
        if (collision.transform.tag == "Enemy")
        {
            TouchingNecro = true;
            if(m_Grounded==true && crouched == false)
            {
                TakeDamage(new Vector2(-transform.localScale.x, 1));
            }
            else if(m_Grounded==false && crouched == false)
            {
                TakeDamage(new Vector2(-transform.localScale.x, 1));
            }
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            TouchingNecro = false;
        }
    }

    //---------------------------------------------------------Coroutines--------------------------------------------------------------------------------//

    IEnumerator Detaching()
        {
        yield return new WaitForSeconds(1f);
        AttachedTo = null;
        }
        IEnumerator DamageCoolDown()
        {
        PlaySound(200);
        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(.1f);
        Time.timeScale = 1f;
        }

        IEnumerator Knockback(Vector2 dir)
        {
            if (HP >= 1)
            {
                knockback = true;
                Vector2 Knockback = new Vector2(1, 1);
                GetComponent<Rigidbody2D>().AddForce(dir * 4, ForceMode2D.Impulse);
                yield return new WaitForSeconds(1f);
                knockback = false;
                Invulnerable = false;
            }
            else
            {
            yield return new WaitForSeconds(1f);
            Invulnerable = false;
            }
        }

        IEnumerator Death()
        {
            GameObject loader = GameObject.Find("LevelLoader/Crossfade");
            m_Anim.SetBool("Death",true);
            m_Rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
            m_Rigidbody2D.simulated = false;
            GameObject.FindObjectOfType<Audiomanager>().fade(0, 1.5f);
            yield return new WaitForSeconds(.5f);
            loader.GetComponent<Animator>().SetTrigger("Start");
            yield return new WaitForSeconds(1f);
            GameObject.FindObjectOfType<Audiomanager>().Stop("Level1");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            dead = false;
            m_Anim.SetBool("Death", false);
            m_Rigidbody2D.simulated = false;
            m_Rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
        }
    }

