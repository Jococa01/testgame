﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Audio : MonoBehaviour
{
    [Range(0, 1)] [SerializeField] public static float AudioMultiplyer=1;
    public Text number;
    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float jacobo = Mathf.Round(AudioMultiplyer * 100f) / 100;
        number.text = jacobo.ToString();
        slider.value = AudioMultiplyer;
    }

    public void ChangeAudio(float newfloat)
    {
        AudioMultiplyer = newfloat;
    }
}
