﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GlobalOptions : MonoBehaviour
{
    [Header("Opciones de Video")]
    public TMPro.TMP_Dropdown QualityDropDown;
    public Resolution[] resolutions;
    public TMPro.TMP_Dropdown ResDropDown;
    public bool IsVSyncEnabled, IsFullscreen;
    static int CurrentResolution;
    static List<string> opciones = new List<string>();

    public GameObject FullscreenToggle, VsyncToggle;
    // Start is called before the first frame update

    private void Awake()
    {
        //Fullscreencheck

        if (Screen.fullScreen == true)
        {
            IsFullscreen = true;
        }
        if (Screen.fullScreen == false)
        {
            IsFullscreen = false;
        }

        //Vsync check

        if (QualitySettings.vSyncCount == 1)
        {
            IsVSyncEnabled = true;
        }
        if (QualitySettings.vSyncCount == 0)
        {
            IsVSyncEnabled = false;
        }
    }
    void Start()
    {
        VsyncChanger(IsVSyncEnabled);
        Fullscreen(IsFullscreen);

        resolutions = Screen.resolutions;
        int settings = QualitySettings.GetQualityLevel();
        QualityDropDown.value = settings;
        ResDropDown.ClearOptions();

        //List<string> opciones = new List<string>();
        int currentresindex = 0;

        if (opciones.Count == 0)
        {
            for (int i = 0; i < resolutions.Length; i++)
            {
                string opcion = resolutions[i].width + "x" + resolutions[i].height + " @" + Screen.currentResolution.refreshRate + "Hz";
                opciones.Add(opcion);

                if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
                {
                    currentresindex = i;
                }
            }

            ResDropDown.AddOptions(opciones);
            ResDropDown.value = currentresindex;
            ResDropDown.RefreshShownValue();
        }
        else if (opciones.Count > 0)
        {
            ResDropDown.AddOptions(opciones);
            ResDropDown.value = CurrentResolution;
            ResDropDown.RefreshShownValue();
        }
    }

    // Update is called once per frame
    void Update()
    {
        CurrentResolution = ResDropDown.value;
    }
    public void QualitySetting(int QualityLevel)
    {
        int settings = QualitySettings.GetQualityLevel();
        settings = QualityLevel;
        switch (settings)
        {
            case 0:
                QualitySettings.SetQualityLevel(0);
                Debug.Log(QualityLevel);
                break;
            case 1:
                QualitySettings.SetQualityLevel(1);
                Debug.Log(QualityLevel);
                break;
            case 2:
                QualitySettings.SetQualityLevel(2);
                Debug.Log(QualityLevel);
                break;
            case 3:
                QualitySettings.SetQualityLevel(3);
                Debug.Log(QualityLevel);
                break;
            case 4:
                QualitySettings.SetQualityLevel(4);
                Debug.Log(QualityLevel);
                break;
            case 5:
                QualitySettings.SetQualityLevel(5);
                Debug.Log(QualityLevel);
                break;
        }
    }
    public void SetWindowResolution(int ResolutionIndex)
    {
        Resolution resolution = resolutions[ResolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
    public void VsyncChanger(bool VsyncIngame)
    {
        IsVSyncEnabled = VsyncIngame;
        if (IsVSyncEnabled == true)
        {
            QualitySettings.vSyncCount = 1;
            VsyncToggle.GetComponent<Toggle>().isOn = true;
        }
        if(IsVSyncEnabled==false)
        {
            QualitySettings.vSyncCount = 0;
            VsyncToggle.GetComponent<Toggle>().isOn = false;
        }
    }
    public void Fullscreen(bool fullscreened)
    {
        IsFullscreen = fullscreened;
        if (IsFullscreen == true)
        {
            Screen.fullScreen = true;
            FullscreenToggle.GetComponent<Toggle>().isOn = true;
        }
        else
        {
            Screen.fullScreen = false;
            FullscreenToggle.GetComponent<Toggle>().isOn = false;
        }
    }
}
