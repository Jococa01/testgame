﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveGCheck : MonoBehaviour
{

    private Rigidbody2D Selfrb;
    private GameObject Player;
    private PlatformerCharacter2D platformer;
    [SerializeField]
    public LayerMask suelo;

    private void Awake()
    {
        Selfrb = transform.GetComponent<Rigidbody2D>();
        Player = GameObject.FindGameObjectWithTag("Player");
        platformer = Player.GetComponent<PlatformerCharacter2D>();
    }

    private void Update()
    {
        if (platformer.dragging == false)
        {
            Physics2D.queriesStartInColliders = false;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, .53f, suelo);

            if (hit.collider != null)
            {
                Selfrb.bodyType = RigidbodyType2D.Static;
            }
            else if (hit.collider == null)
            {
                Selfrb.bodyType = RigidbodyType2D.Dynamic;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.down *.53f);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Limitador")
        {
            if (platformer.dragging == true)
            {
                platformer.dragging = false;
                platformer.m_Anim.SetBool("IsPushing", false);
                GetComponent<FixedJoint2D>().enabled = false;
            }
        }
    }

}
