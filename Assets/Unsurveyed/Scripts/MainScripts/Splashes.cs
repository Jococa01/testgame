﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Splashes : MonoBehaviour
{
    public float Tiempo;
    public GameObject Load;
    private void Awake()
    {
        Load = GameObject.Find("LevelLoader");
        StartCoroutine(WaitForVideo(Tiempo));
    }
    void SplashPlayer()
    {

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(WaitForVideo(0f));
        }
    }

    IEnumerator WaitForVideo(float time)
    {
        yield return new WaitForSeconds(time);

        Load.GetComponentInChildren<Animator>().SetTrigger("Start");

        yield return new WaitForSeconds(1f);

        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
