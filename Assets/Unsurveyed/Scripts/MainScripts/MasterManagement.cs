﻿using UnityEngine;

public class MasterManagement : MonoBehaviour
{
    private static GameObject _instance;

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this.gameObject;
            DontDestroyOnLoad(this.gameObject);

            //Rest of your Awake code

        }
        else
        {
            Destroy(this);
        }
    }

    //Rest of your class code
}
