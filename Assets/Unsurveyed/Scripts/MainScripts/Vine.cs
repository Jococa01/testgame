﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vine : MonoBehaviour
{

    public Rigidbody2D VineParent;
    public GameObject[] PrefabVineSegs;
    public int NumberOfLinks = 5;

    public HingeJoint2D top;
    // Start is called before the first frame update
    void Start()
    {
        GenerateVine();
    }
    void GenerateVine()
    {
        Rigidbody2D prevBody = VineParent;
        for(int i =0; i< NumberOfLinks; i++)
        {
            int index = Random.Range(0, PrefabVineSegs.Length);
            GameObject NewSegment = Instantiate(PrefabVineSegs[index]);
            NewSegment.transform.parent = transform;
            NewSegment.transform.position = transform.position;
            HingeJoint2D HJ = NewSegment.GetComponent<HingeJoint2D>();
            HJ.connectedBody = prevBody;

            prevBody = NewSegment.GetComponent<Rigidbody2D>();

            if (i == 0)
            {
                top = HJ;
            }
        }
    }

    public void Addlink()
    {
        int index = Random.Range(0, PrefabVineSegs.Length);
        GameObject newLink = Instantiate(PrefabVineSegs[index]);
        newLink.transform.parent = transform;
        newLink.transform.position = transform.position;
        HingeJoint2D hj = newLink.GetComponent<HingeJoint2D>();
        hj.connectedBody = VineParent;
        newLink.GetComponent<Segment>().ConnectedBelow = top.gameObject;
        top.connectedBody = newLink.GetComponent<Rigidbody2D>();
        top.GetComponent<Segment>().ResetAnchor();
        top = hj;

    }

    public void RemoveLink()
    {
        HingeJoint2D newtop = top.gameObject.GetComponent<Segment>().ConnectedBelow.GetComponent<HingeJoint2D>();
        newtop.connectedBody = VineParent;
        newtop.gameObject.transform.position = VineParent.gameObject.transform.position;
        newtop.GetComponent<Segment>().ResetAnchor();
        Destroy(top.gameObject);
        top = newtop;
    }
}
