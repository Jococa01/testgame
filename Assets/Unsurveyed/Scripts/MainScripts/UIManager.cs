﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public Slider StaminaBar;
    public bool OptionsIsActive = false;
    public static List<GameObject> PuzzlePieces = new List<GameObject>();
    public Scene NombreEscena;
    public static int SceneNumber;
    public int i;
    public GameObject CurrencyText, InvMenu, optionsmenu, videoptions,popup;
    public bool IsPaused = false;

    public void Start()
    {
        CurrencyText = GameObject.Find("UI/Canvas/AlwaysOnTop/PlayerStats/Currency/CurrencyText");
        NombreEscena = SceneManager.GetActiveScene();
        SceneNumber = SceneManager.GetActiveScene().buildIndex;
    }

    public void Update()
    {
        CurrencyText.GetComponent<Text>().text = "x " + PlatformerCharacter2D.CoinCount.ToString();
        if (Input.GetKeyDown(KeyCode.Escape) && OptionsIsActive==false && videoptions.activeSelf==false)
        {
            if (InvMenu.activeSelf == false)
            {
                InvMenu.SetActive(true);
                Time.timeScale = 0f;
                IsPaused = true;
            }
            else if (InvMenu.activeSelf == true)
            {
                InvMenu.SetActive(false);
                Time.timeScale = 1f;
                IsPaused = false;
            }
        }
        if(Input.GetKeyDown(KeyCode.Escape) && OptionsIsActive == true && videoptions.activeSelf==false)
        {
            optionsmenu.SetActive(false);
            OptionsIsActive = false;
            InvMenu.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Escape) && videoptions.activeSelf == true)
        {
            videoptions.SetActive(false);
            OptionsIsActive = true;
            optionsmenu.SetActive(true);
        }
        if(Input.GetKeyDown(KeyCode.Escape) && popup.activeSelf == true)
        {
            popup.SetActive(false);
            popup.GetComponent<PuzzleMenu>().ResetPuzzlePieces();
            InvMenu.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    //Voids for some data
    public void SetMaxStamina(int Stamina)
    {
        StaminaBar.maxValue = Stamina;
        StaminaBar.value = Stamina;
    }
    public void SetStamina(float Stamina)
    {
        StaminaBar.value = Stamina;
    }
    public void BoolChanger()
    {
        OptionsIsActive = !OptionsIsActive;
        Debug.Log("Cambio el valor de la booleana");
    }
    public void DefaultTimeScale()
    {
        OptionsIsActive = false;
        Time.timeScale = 1f;
    }
}
