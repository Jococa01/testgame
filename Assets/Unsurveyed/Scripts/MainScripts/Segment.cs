﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour
{
    public GameObject ConnectedAbove, ConnectedBelow;
    public bool isPlayerAttached;
    // Start is called before the first frame update
    void Start()
    {
        ResetAnchor();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResetAnchor()
    {
        ConnectedAbove = GetComponent<HingeJoint2D>().connectedBody.gameObject;
        Segment abovesegment = ConnectedAbove.GetComponent<Segment>();
        if (abovesegment != null)
        {
            abovesegment.ConnectedBelow = gameObject;
            float SpriteBottom = ConnectedAbove.GetComponent<SpriteRenderer>().bounds.size.y;
            GetComponent<HingeJoint2D>().connectedAnchor = new Vector2(0, SpriteBottom * -1f);
        }
        else
        {
            GetComponent<HingeJoint2D>().connectedAnchor = new Vector2(0, 0);
        }
    }
}
