﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MController : MonoBehaviour
{
    public GameObject Options, VideoOptions, MainMenu;

    public GameObject BackButton, optionsfirstbutton, firstbutton;

    public void Awake()
    {
        
    }

    public void ActiveMenu(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            Debug.Log("Start");
        }
    }
    public void Back(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            Debug.Log("Back");
            BackButton = GameObject.Find("Back");
            BackButton.GetComponent<Button>().Select();
        }
    }
    public void Navigation(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            Debug.Log("Joystick");
        }
    }

    public void OnOptionsMenu()
    {
        if (!Options.activeInHierarchy)
        {
            Options.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(optionsfirstbutton);
        }
    }
}
