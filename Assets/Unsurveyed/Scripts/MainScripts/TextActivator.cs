﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextActivator : MonoBehaviour
{
    Animator anim;
    public string dialoguetext;
    public DialogueWindows dialogue;
    public GameObject InteractText;
    [SerializeField]
    public GameObject VisualInteraction;

    private void Start()
    {
        anim = transform.GetChild(0).GetComponent<Animator>();
        InteractText.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
                InteractText.SetActive(true);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                anim.SetBool("Exit", true);
                Debug.Log("MEEm");
                InteractText.SetActive(false);
                dialogue.Show(dialoguetext);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            anim.SetBool("Exit", false);
            InteractText.SetActive(false);
            dialogue.Close();
        }
    }
}
