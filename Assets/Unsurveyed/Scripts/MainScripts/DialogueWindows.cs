﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueWindows : MonoBehaviour
{
    const string HTMLColor = "<color=#00000000>";
    public TMP_Text Text;
    private string CurrentText;

    Animator group;
    
    // Start is called before the first frame update
    void Start()
    {
        Text.enabled = false;
        group = GetComponent<Animator>();
    }

    public void Show(string text)
    {
        group.SetBool("Open", true);
        CurrentText = text;
        
    }
    public void Close()
    {
        
        group.SetBool("Open", false);
    }
    public void Opening()
    {
        Text.enabled = true;
        StartCoroutine(DisplayText());
    }
    public void Closing()
    {
        StopAllCoroutines();
        Text.enabled = false;
    }

    IEnumerator DisplayText()
    {
        Text.text = "";

        string originaltext = CurrentText;
        string displayedtext = "";
        int alphaindex = 0;

        foreach(char c in CurrentText.ToCharArray())
        {
            alphaindex++;
            Text.text = originaltext;
            displayedtext = Text.text.Insert(alphaindex, HTMLColor);
            Text.text = displayedtext;
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }
}
