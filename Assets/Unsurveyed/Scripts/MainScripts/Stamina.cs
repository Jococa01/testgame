﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stamina : MonoBehaviour
{
    public UIManager UIManager;
    public PlatformerCharacter2D PlatformerCharacter2D;
    public float PossessionTimer;

    // Start is called before the first frame update
    private void Awake()
    {
        PlatformerCharacter2D = FindObjectOfType<PlatformerCharacter2D>();
        UIManager = FindObjectOfType<UIManager>();

        PlatformerCharacter2D.PossesionTime = 10f;
        PossessionTimer = PlatformerCharacter2D.PossesionTime;
    }

    // Update is called once per frame
    void Update()
    {
        UIManager.SetStamina(PossessionTimer);
        if (PlatformerCharacter2D.IsPossessing == false)
        {
            if (PossessionTimer <= 10)
            {
                PossessionTimer += .5f *Time.deltaTime;
            }
        }
        if (PossessionTimer <= 0)
        {
            PossessionTimer = 0f;
            PlatformerCharacter2D.WaitTimeIsOver = false;
            Debug.Log(PossessionTimer +" y " + PlatformerCharacter2D.PossesionTime);
        }
        else if (PossessionTimer > 0)
        {
            PlatformerCharacter2D.WaitTimeIsOver = true;
        }
    }
}
