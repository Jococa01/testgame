﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagere : MonoBehaviour
{
    public int MainScene;

    private void Awake()
    {
        MainScene = SceneManager.GetActiveScene().buildIndex;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            SceneManager.LoadScene(MainScene+1);
        }
    }
}
