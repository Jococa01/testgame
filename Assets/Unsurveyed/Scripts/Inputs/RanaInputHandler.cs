﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class RanaInputHandler : MonoBehaviour
{
    public bool IsPossessed = false;
    public float Force, Xdir;
    public GameObject Player;
    public Rigidbody2D rb;
    public Vector3 movementinput;
    private Vector3 LookDir;
    [SerializeField]
    public LayerMask WhatIsGround;
    public RanaToro Enemybase;
    public Animator animator;
    public CinemachineVirtualCamera vcam;

    //StaminaBar
    public Stamina stamina;

    //possession mechanic
    public Entity entity;
    public bool Reset = false;

    //Audio
    public AudioSource source;
    public AudioClip[] clips;
    public AudioListener listener;

    private void Awake()
    {
        entity = GameObject.Find("Parámetros Rana Toro").GetComponent<Entity>();
        animator = transform.GetComponent<Animator>();
        Xdir = transform.localScale.x;
        stamina = FindObjectOfType<Stamina>();
        source = GetComponent<AudioSource>();
        listener = GetComponent<AudioListener>();
    }
    public void Update()
    {

        if (IsPossessed == true)
        {
            GetComponent<SpriteRenderer>().sprite = Player.GetComponent<PlatformerCharacter2D>().PossessedSprites[2];
            animator.SetBool("charge", false);
            Enemybase.enabled = false;
            Player.GetComponent<PlatformerCharacter2D>().IsPossessing = true;
            Player.SetActive(false);
            ResetMovementInput();
            rb.bodyType = RigidbodyType2D.Dynamic;
            //LookDir = new Vector3(Xdir, 1, 1);
            vcam.m_Follow = transform;
            stamina.PossessionTimer -= Time.deltaTime;
            listener.enabled = true;

            //transform.localScale = LookDir;

            if (stamina.PossessionTimer <= 0)
            {
                IsPossessed = false;
                listener.enabled = false;
                movementinput.x = 1;
                Reset = true;
                Enemybase.enabled = true;
                Player.SetActive(true);
                vcam.m_Follow = Player.transform;
                Player.GetComponent<PlatformerCharacter2D>().IsPossessing = false;
                Player.GetComponent<AudioListener>().enabled = true;
                transform.GetComponent<PlayerInput>().enabled = false;
                Player.GetComponent<PlayerInput>().enabled = true;
                Debug.Log("El tiempo es " + stamina.PossessionTimer);
            }
            if (movementinput.x > 0)
            {
                transform.Translate(Vector3.right * Time.deltaTime * 3f, Space.World);
                transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
                //entity.XDir = transform.localScale.x;
                animator.SetBool("move", true);
                animator.SetBool("idle", false);
            }
            if (movementinput.x < 0)
            {
                transform.Translate(Vector3.left * Time.deltaTime * 3f, Space.World);
                transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
                //entity.XDir = transform.localScale.x;
                animator.SetBool("move", true);
                animator.SetBool("idle", false);
            }
            else if (movementinput.x == 0)
            {
                animator.SetBool("idle", true);
                animator.SetBool("move", false);
            }

        }
        else
        {
            //Xdir = transform.localScale.x;
        }
    }

    public void OnMoveInput(InputAction.CallbackContext context)
    {
        movementinput = context.ReadValue<Vector2>();

        if (context.started)
        {
            if (movementinput.x > 0)
            {
                Xdir = 1;
            }
            if (movementinput.x < 0)
            {
                Xdir = -1;
            }
        }
    }
    public void OnExitPossessionInput(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            IsPossessed = false;
            listener.enabled = false;
            movementinput.x = 1;
            Reset = true;
            animator.SetBool("move", true);
            animator.SetBool("idle", false);
            Enemybase.enabled = true;
            Player.SetActive(true);
            vcam.m_Follow = Player.transform;
            Player.GetComponent<PlatformerCharacter2D>().IsPossessing = false;
            Player.GetComponent<AudioListener>().enabled = true;
            transform.GetComponent<PlayerInput>().enabled = false;
            Player.GetComponent<PlayerInput>().enabled = true;
            Debug.Log("El tiempo es " + stamina.PossessionTimer);
        }
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.down * .85f);
        //Gizmos.DrawWireSphere(AttackPlace.transform.position, 1f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Destructible" && rb.velocity.x>2)
        {
            collision.transform.GetComponent<Animator>().SetBool("Break", true);
            Destroy(collision.gameObject);
        }
    }

    public void ResetMovementInput()
    {
        if (Reset == true)
        {
            movementinput.x = 0;
            Debug.Log("reseted");
            Reset = false;
        }
        animator.SetBool("move", false);
    }
    public void PlaySound(int sound)
    {
        source.clip = clips[sound];
        source.Play();
        if (sound == 1)
        {
            source.pitch = 1.4f;
            source.spatialBlend = 1f;
            source.maxDistance = 20f;
            source.minDistance = 1f;
            source.volume = .8f;
        }
        if (sound == 0)
        {
            source.pitch = 1f;
            source.spatialBlend = 1f;
            source.maxDistance = 20f;
            source.minDistance = 1f;
            source.volume = .8f;
        }
    }

    //IEnumerator ChargeBreak()
    //{
    //    collision.transform.GetComponent<Animator>().SetBool("Break", true);
    //    yield return new WaitForSeconds
    //}
}
