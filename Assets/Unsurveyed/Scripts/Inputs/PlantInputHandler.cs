﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class PlantInputHandler : MonoBehaviour
{
    public bool IsPossessed = false, Grounded = false;
    public float Force, Xdir;
    public GameObject Player;
    public Rigidbody2D rb;
    public Vector3 movementinput;
    private Vector3 LookDir;
    [SerializeField]
    public LayerMask WhatIsGround;
    public Flor Enemybase;
    public Animator animator;
    public CinemachineVirtualCamera vcam;

    //Ataque del Necromorfo
    //public GameObject AttackPlace;
    [SerializeField]
    public LayerMask PlayerMask;

    //StaminaBar
    public Stamina stamina;

    //possession mechanic
    public Entity entity;
    public bool Reset = false;

    //Audio
    public AudioSource source;
    public AudioClip[] clips;
    public AudioListener listener;

    private void Awake()
    {
        entity = transform.parent.gameObject.GetComponent<Entity>();
        animator = transform.GetComponent<Animator>();
        Xdir = transform.localScale.x;
        stamina = FindObjectOfType<Stamina>();
        source = GetComponent<AudioSource>();
        listener = GetComponent<AudioListener>();
    }
    public void Update()
    {
        Physics2D.queriesStartInColliders = false;
        RaycastHit2D hit2D = Physics2D.Raycast(transform.position, Vector2.down, .85f, WhatIsGround);
        if (hit2D.collider != null)
        {
            Grounded = true;
        }
        else
        {
            Grounded = false;
        }

        if (IsPossessed == true)
        {
            listener.enabled = true;
            ResetMovementInput();
            animator.SetBool("shoot", false);
            rb.bodyType = RigidbodyType2D.Dynamic;
            LookDir = new Vector3(Xdir, 1, 1);
            vcam.m_Follow = transform;
            Enemybase.enabled = false;
            Player.GetComponent<PlatformerCharacter2D>().IsPossessing = true;
            Player.SetActive(false);
            stamina.PossessionTimer -= Time.deltaTime;

            transform.localScale = LookDir;

            if (stamina.PossessionTimer <= 0)
            {
                IsPossessed = false;
                listener.enabled = false;
                movementinput.x = 1;
                Reset = true;
                animator.SetBool("idle", true);
                Enemybase.enabled = true;
                Player.GetComponent<PlatformerCharacter2D>().IsPossessing = false;
                Player.GetComponent<AudioListener>().enabled = true;
                transform.GetComponent<PlayerInput>().enabled = false;
                Player.GetComponent<PlayerInput>().enabled = true;
                Debug.Log("El tiempo es " + stamina.PossessionTimer);
            }
            if (movementinput.x > 0)
            {
                transform.Translate(Vector3.right * Time.deltaTime * 3f, Space.World);
                entity.XDir = transform.localScale.x;
                animator.SetBool("move", true);
                animator.SetBool("idle", false);
            }
            if (movementinput.x < 0)
            {
                transform.Translate(Vector3.left * Time.deltaTime * 3f, Space.World);
                entity.XDir = transform.localScale.x;
                animator.SetBool("move", true);
                animator.SetBool("idle", false);
            }
            else if (movementinput.x == 0)
            {
                animator.SetBool("idle", true);
                animator.SetBool("move", false);
            }

        }
        else
        {
            if (Grounded == true)
            {
                rb.bodyType = RigidbodyType2D.Kinematic;
            }
            else
            {
                rb.bodyType = RigidbodyType2D.Dynamic;
                rb.simulated = true;
            }
            Player.SetActive(true);
            vcam.m_Follow = Player.transform;
            Xdir = transform.localScale.x;
        }
    }

    //public void OnMoveInput(InputAction.CallbackContext context)
    //{
    //    movementinput = context.ReadValue<Vector2>();

    //    if (context.started)
    //    {
    //        if (movementinput.x > 0)
    //        {
    //            Xdir = 1;
    //        }
    //        if (movementinput.x < 0)
    //        {
    //            Xdir = -1;
    //        }
    //    }
    //}

    public void OnShootInput(InputAction.CallbackContext context)
    {
        if (IsPossessed)
        {
            if (context.started && Grounded == true)
            {
                rb.AddForce(new Vector2(0, 1 * Force));
                Debug.Log("Salto con el enemigo poseído");
            }
        }
    }

    public void OnExitPossessionInput(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            IsPossessed = false;
            listener.enabled = false;
            movementinput.x = 1;
            Reset = true;
            animator.SetBool("move", true);
            Enemybase.enabled = true;
            Player.GetComponent<PlatformerCharacter2D>().IsPossessing = false;
            Player.GetComponent<AudioListener>().enabled = true;
            transform.GetComponent<PlayerInput>().enabled = false;
            Player.GetComponent<PlayerInput>().enabled = true;
            Debug.Log("El tiempo es " + stamina.PossessionTimer);
        }
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.down * .85f);
        //Gizmos.DrawWireSphere(AttackPlace.transform.position, 1f);
    }

    public void DoDamage()
    {
        //Vector3 pos = AttackPlace.transform.position;
        //Physics2D.queriesStartInColliders = false;
        //Collider2D colInfo = Physics2D.OverlapCircle(pos, 1f, PlayerMask);
        //if (colInfo != null)
        //{
        //    Player.GetComponent<PlatformerCharacter2D>().TakeDamage();
        //    Debug.Log("Ejecuto el activador de daño");
        //}
        //else
        //{

        //}
    }
    public void ResetMovementInput()
    {
        if (Reset == true)
        {
            movementinput.x = 0;
            Debug.Log("reseted");
            Reset = false;
        }
    }
    public void PlaySound(int sound)
    {
        source.clip = clips[sound];
        source.Play();
        if (sound == 1)
        {
            source.pitch = 1.4f;
            source.spatialBlend = 1f;
            source.maxDistance = 20f;
            source.minDistance = 1f;
            source.volume = .8f;
        }
        if (sound == 0)
        {
            source.pitch = 1f;
            source.spatialBlend = 1f;
            source.maxDistance = 20f;
            source.minDistance = 1f;
            source.volume = .8f;
        }
    }
}
