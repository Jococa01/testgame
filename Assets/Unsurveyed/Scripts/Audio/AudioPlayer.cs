﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    [SerializeField]
    public List<string> Canciones = new List<string>();
    public int NumCancion;
    public Audiomanager manager;
    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<Audiomanager>();
        foreach(Sound s in manager.sounds)
        {
            Canciones.Add(s.name);
            s.volume = s.volume * Audio.AudioMultiplyer;
        }
        manager.Play(Canciones[NumCancion]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
