﻿using UnityEngine.Audio;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using UnityEngine;
using System.Collections;

public class Audiomanager : MonoBehaviour
{
    public Sound[] sounds;
    public static Audiomanager instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume * Audio.AudioMultiplyer;
            s.source.pitch = s.pitch;
            s.source.loop = s.Loop;
            s.source.spatialBlend = s.SpatialBlend;
            s.source.maxDistance = s.MaxRange;
        }

    }
    private void Start()
    {

    }
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            return;
        }
        s.source.Play();

    }
    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            return;
        }
        s.source.Stop();
    }
    public void fade(int AudioID, float time)
    {
        StartCoroutine(StartFade(AudioID,time));
    }
    IEnumerator StartFade(int AudioID,float time)
    {
        float currentTime = 0;
        float start = sounds[AudioID].source.volume;

        while (currentTime < time)
        {
            currentTime += Time.deltaTime;
            sounds[AudioID].source.volume = Mathf.Lerp(start, 0, currentTime / time);
            yield return null;
        }
        yield break;
    }
    public void FadeStartGame(int AudioID)
    {
        float time = 1;
        StartCoroutine(StartFade(AudioID, time));
    }
}

