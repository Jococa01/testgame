﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameData_S : MonoBehaviour
{
    protected GameObject Player;
    public bool HasSaved = false;

    [Header("Lista de Objectos con Interacciones")]
    public GameObject[] MovableObjs;
    public List<Vector3> MovList = new List<Vector3>();
    public int ListSize;

    private void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        MovableObjs = GameObject.FindGameObjectsWithTag("Interactive");

        /*Guardando las coordenadas de múltiples elementos
        Selecciono con que tipo de objetos quiero trabajar y almaceno sus datos en un Array
        Para cada objeto dentro del conjunto que he creado le asigno una variable local que me permite trabajar con cada objeto de forma individual
        Luego cojo el valor de la posición de cada objeto y lo almaceno en un listado de Vectores*/

        foreach (GameObject Obj in MovableObjs)
        {
            MovList.Add(Obj.transform.position);
            Debug.Log(Obj.name + " es el objeto º" + MovList.IndexOf(Obj.transform.position));
        }

        ListSize = MovableObjs.Length;

    }
    public void Save()
    {
        PlatformerCharacter2D PlayerScript = Player.GetComponent<PlatformerCharacter2D>();
        ES3.Save("playerpos",Player.transform.position);
        ES3.Save("Scene", SceneManager.GetActiveScene().buildIndex);

        for (int i = 0; i < ListSize && i < MovableObjs.Length; i++)
        {
            MovList[i] = MovableObjs[i].transform.position;
        }

        ES3.Save("InteractivePos", MovList);
    }
}
