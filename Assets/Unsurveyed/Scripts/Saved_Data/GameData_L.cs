﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameData_L : MonoBehaviour
{
    protected GameObject Player;
    public bool hasaved;

    [Header("Lista de Objectos con Interacciones")]
    public GameObject[] MovableObjs;
    public List<Vector3> MovList = new List<Vector3>();

    [Header("Variables Importantes")]
    public int ListSize;

    private void Awake()
    {
        Time.timeScale = 1f;
        Player = GameObject.FindGameObjectWithTag("Player");
        MovableObjs = GameObject.FindGameObjectsWithTag("Interactive");
        ListSize = MovableObjs.Length;
        hasaved = ES3.Load<bool>("hasSaved");

        if (LoadScene.LoadingScene == true)
        {
            Load();
        }
    }
    public void Load()
    {
        PlatformerCharacter2D PlayerScript = Player.GetComponent<PlatformerCharacter2D>();
        Player.transform.position = ES3.Load<Vector3>("playerpos");
        MovList = ES3.Load("InteractivePos", MovList);

        for(int i =0; i < ListSize && i < MovableObjs.Length; i++)
        {
            MovableObjs[i].transform.position = MovList[i];
            Debug.Log("He movido " + MovableObjs[i].name + " a " + MovList[i]);
        }
        LoadScene.LoadingScene = false;
    }
    public void ToMC()
    {
        SceneManager.LoadSceneAsync(1);
        Time.timeScale = 1f;
    }
}
