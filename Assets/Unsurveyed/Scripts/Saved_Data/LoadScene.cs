﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public static bool LoadingScene = false;
    public GameObject Load;

    private void Start()
    {
        Load = GameObject.Find("LevelLoader");
    }
    public void LoadScenes()
    {
        SceneManager.LoadScene(ES3.Load<int>("Scene"));
        LoadingScene = true;
        if (ES3.KeyExists("Scene")==false)
        {
            Debug.Log("No hay partidas guardadas");
        }
    }
    public void NewGame()
    {
        //Cuando tenga más de 1 escena tengo que cambiar este código para que resetee todas las variables a guardar
        ES3.Save("Scene", SceneManager.GetActiveScene().buildIndex+1);
        StartCoroutine(loadlevel());
    }
    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator loadlevel()
    {
        Load.GetComponentInChildren<Animator>().SetTrigger("Start");

        yield return new WaitForSeconds(1f);

        SceneManager.LoadSceneAsync(ES3.Load<int>("Scene"));
    }
}
