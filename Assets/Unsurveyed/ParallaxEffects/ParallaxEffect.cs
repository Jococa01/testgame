﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    //Antiguo
    //private float Length;
    //private float StartPos;
    //public float ParallaxEFF;

    //Nuevo

    //Variables numéricas
    public float BGSize;

    //Se ha de ajustar en función a la distanca del nivel;
    [Header("Ajustes Sprites")]
    public float FieldView = 25f;
    public int DeepZ;

    private int LeftIndex;
    private int RightIndex;

    public float ParallaxSpeed;
    private float LastCameraX;

    //Otras
    private Transform CameraTransform;
    private Transform[] Layers;

    //Booleanas de ajuste;
    [Header("Activaciones")]
    public bool ModoParallax = true;
    public bool ScrollDeSprites = true;



    // Start is called before the first frame update
    void Start()
    {

        //Iniciadores, adquiere la posición de la cámara en la cual se basará para mover los sprites.
        CameraTransform = Camera.main.transform;
        LastCameraX = CameraTransform.position.x;

        //Cuenta el número de hijos del padre especificado;
        Layers = new Transform[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        Layers[i] = transform.GetChild(i);

        LeftIndex = 0;
        RightIndex = Layers.Length - 1;

        //Antiguo.
        //StartPos = transform.position.x;
        //Length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    private void ScrollLeft()
    {
        
        //Estable la siguiente posición del sprite respecto a la izquierda de la cámara.
        Layers[RightIndex].position = new Vector3 (1*Layers[LeftIndex].position.x - BGSize,Layers[LeftIndex].position.y,DeepZ);
        LeftIndex = RightIndex;
        RightIndex--;

        //Movimiento Izquierda, crea el fondo con infinito desplazamiento cada sprite hijo a la dirección a la cual se mueve el personaje.
        if (RightIndex < 0)
            RightIndex = Layers.Length - 1;
    }

    private void ScrollRight()
    {
        
        //Estable la siguiente posición del sprite respecto a la derecha de la cámara.
        Layers[LeftIndex].position = new Vector3(1*Layers[RightIndex].position.x + BGSize, Layers[RightIndex].position.y, DeepZ);
        RightIndex = LeftIndex;
        LeftIndex++;

        //Movimiento Derecha, crea el fondo con infinito desplazamiento cada sprite hijo a la dirección a la cual se mueve el personaje.
        if (LeftIndex == Layers.Length)
            LeftIndex = 0;
    }





    // Update is called once per frame
    void Update()
    {
        if (ModoParallax)
        {
            //Reinicio de la posición en z de la cámara.
            float DeltaX = CameraTransform.position.x - LastCameraX;
            transform.position += Vector3.right * (DeltaX * ParallaxSpeed);
        }
        

        LastCameraX = CameraTransform.position.x;


        if (ScrollDeSprites)
        {
            //Moviemento de los sprites hacia la posición indicada.
            if (CameraTransform.position.x < (Layers[LeftIndex].transform.position.x + FieldView))
                ScrollLeft();

            if (CameraTransform.position.x > (Layers[RightIndex].transform.position.x - FieldView))
                ScrollRight();
        }
        


        //Antiguo

        /*float Temp = Camera.main.transform.position.x * (1 - ParallaxEFF);
        float Distance = Camera.main.transform.position.x * ParallaxEFF;

        transform.position = new Vector3(StartPos + Distance, transform.position.y, transform.position.z);

        if (Temp > StartPos + Length)
        {
            StartPos += Length;
        }

        else if(Temp < StartPos - Length)
        {
            StartPos -= Length;
        }*/
    }
}