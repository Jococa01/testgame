﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(FOV))]
public class VisionFOV : Editor
{
    public Vector3 Laterales = new Vector3(0,0,1);
    private void OnSceneGUI()
    {
        FOV fov = (FOV)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(fov.transform.position, Vector3.forward, Vector3.up, 360,fov.Radiovision);
        Vector3 VerAnguloA = fov.Direccionangular(-fov.Angulovision / 2, false);
        Vector3 VerAnguloB = fov.Direccionangular(fov.Angulovision / 2, false);

        Handles.DrawLine(fov.transform.position,fov.transform.position + VerAnguloA*fov.Radiovision*fov.transform.localScale.x);
        Handles.DrawLine(fov.transform.position, fov.transform.position + VerAnguloB * fov.Radiovision * fov.transform.localScale.x);

        Handles.color = Color.red;
        foreach (Transform visibletarget in fov.VisibleTargets)
        {
            Handles.DrawLine(fov.transform.position, visibletarget.position);
        }
    }

}
